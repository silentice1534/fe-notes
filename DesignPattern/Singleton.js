/**
 * 保證只有一個實例，並提供一個存取它的全域存取點
 *
 * 1. 利用物件做一個namespace
 * 2. 利用閉包實作單例模式
 * 3. 惰性單例
 */

/**
 * 利用物件做一個namespace(就是平常在做的事情)
 */

// e.g.
const onlyObj = {
  a: '',
  b: {
    c: '',
  },
};

// 動態建立物件
let dynamicObj = {};

function createObjNode(props) {
  const propsArr = props.split('.');
  currDynamicObj = dynamicObj;
  for (let i in propsArr) {
    if (!currDynamicObj[propsArr[i]]) {
      currDynamicObj[propsArr[i]] = {};
    }
    currDynamicObj = currDynamicObj[propsArr[i]];
  }
}

createObjNode('a');
createObjNode('b.a');

console.log(dynamicObj);
/**
 * dynamicObj = {
 *   a: {},
 *   b: {
 *     a: {}
 *   }
 * }
 */

// --------------------------------------------------

/**
 * 利用閉包實作單例模式
 */

const person = (function closureVar() {
  let Obj = {
    name: 'Andy',
    gender: 'female',
  };
  return function() {
    return Obj.name + ' ' + Obj.gender;
  };
})();

console.log(person());

/**
 * 惰性單例
 *
 * 等到真的需要時在建立，且在執行並不會重新建立新的，基本上也是透過閉包實現
 */

// 判斷單例是否存在，若存在返回，不存在就建立
const getSingleton = function(fn) {
  let instance;

  return function() {
    return instance || (instance = fn.apply(this, arguments));
  };
};

// 那個唯一的單例
function createDiv() {
  const div = document.createElement('div');
  div.style.display = 'none';
  div.style.width = '200px';
  div.style.height = '200px';
  div.style.backgroundColor = '#00ff00';
  document.body.appendChild(div);

  return div;
}

// 這個就是對外的唯一存取點
const createSingleElement = getSingleton(createDiv);

// 觸發時
btn.addEventListener('click', function() {
  const ele = createSingleElement();
  console.log(ele);
  ele.style.display = 'block';
});
