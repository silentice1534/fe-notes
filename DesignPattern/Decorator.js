// 物件導向的裝飾者模式

// const Plane = function() {};

// Plane.prototype.fire = function() {
//   console.log('普通子彈');
// };

// const MissileDecorator = function(plane) {
//   this.plane = plane;
// };

// MissileDecorator.prototype.fire = function() {
//   this.plane.fire();
//   console.log('導彈');
// };

// const AtomDecorator = function(plane) {
//   this.plane = plane;
// };

// AtomDecorator.prototype.fire = function() {
//   this.plane.fire();
//   console.log('原子彈');
// };

// let plane = new Plane();
// // plane.fire(); // '普通子彈'

// plane = new MissileDecorator(plane);
// // plane.fire(); // '普通子彈' '導彈'

// plane = new AtomDecorator(plane);
// plane.fire(); // '普通子彈' '導彈' '原子彈'

// JS的裝飾者模式

const plane = {
  fire: function() {
    console.log('普通子彈');
  }
};

const fireMissle = function() {
  console.log('導彈');
};

const fireAtom = function() {
  console.log('原子彈');
};

plane.fire(); // '普通子彈'

const fire1 = plane.fire;

plane.fire = function() {
  fire1();
  fireMissle();
};

plane.fire(); // '普通子彈' '導彈'

const fire2 = plane.fire;

plane.fire = function() {
  fire2();
  fireAtom();
};

plane.fire(); // '普通子彈' '導彈' '原子彈'
