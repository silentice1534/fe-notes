/**
 * 透過一個代理（中介）函式前先處理一些事情，沒問題才處理真正要做的事情。
 *
 * 例如 A想送花給S，但A不敢，於是請B送花給S，這裡的B就是代理函式。
 *
 * 從A到S的過程中，可以過濾掉一些情況，在B即可處理，稱為保護代理；
 * 從A從S的過程中，若B是一個很消耗資源的過程，可以等真的有需求時再開始處理，稱為虛擬代理。
 *
 * 代理函式的介面與本體一樣，所以代理函式某方面來說是個多餘、錦上添花的功能，
 * 隨時拔除也不影響（處理錯誤處理不一樣）。
 *
 * 廣義來看，代理模式就是要處理例外或是一些需要花費時間的事情。
 *
 * 1. 虛擬代理
 * 2. 保護代理
 * 3. 快取代裡
 */

/**
 * 虛擬代理
 * e.g. 圖片載入完成前，先插入loading圖，待圖片載入完成在插入真正的圖片(虛擬代理)
 */

const loadingImg = (function() {
  const imgElement = document.createElement('img');
  console.log(document, document.body);

  document.body.appendChild(imgElement);

  return {
    // 這個是本體介面，會和代理的一樣
    setImageSrc: function(imgPath) {
      imgElement.src = imgPath;
    },
  };
})();

const proxyLoadingImg = (function() {
  const img = new Image();
  img.onload = function() {
    loadingImg.setImageSrc(this.src);
  };

  return {
    // 這個是代理介面，會和本體的一樣
    setImageSrc: function(imgPath) {
      loadingImg.setImageSrc('./img/image.jpeg');
      img.src = imgPath;
    },
  };
})();

proxyLoadingImg.setImageSrc(
  'https://www.nasa.gov/sites/default/files/thumbnails/image/plasmatourhr_orbit1.0090.jpg',
);

/**
 * 保護代理
 * e.g. 透過Ajax請求回傳結果，若有錯誤則不處理後續（保護代理)
 */

/**
 * 快取代裡
 * e.g. 許多重複動作，如果第二次執行了相同事情，可快取起來（快取代裡）
 */

function plusNumber() {
  let total = 0;
  for (i = 0; i < arguments.length; i++) {
    total += arguments[i];
  }

  return total;
}

console.log(plusNumber(1, 2, 3));

const cacheProxyPlusNumber = (function() {
  const cache = {};

  return function() {
    const cacheProp = Array.prototype.join.call(arguments, ',');

    if (cacheProp in cache) {
      console.log('is cache!');
      return cache[cacheProp];
    }

    console.log('no cache!');
    return (cache[cacheProp] = plusNumber.apply(null, arguments));
  };
})();

console.log(cacheProxyPlusNumber(1, 2, 3)); // --> no cache!, 6
console.log(cacheProxyPlusNumber(1, 2, 3)); // --> is cache!, 6

/**
 * 高階函式動態建立代理
 */
