/**
 * 對象的行為在不同場景中有不同的算法。
 * 定義一系列的演算法，並且封裝起來並使他們可以相互替換。
 *
 * 以下例子以所得稅計算當例子
 *                     Tax Rate   Level
 *          ~540,000      5%        D
 * 540,001  ~1,210,000   12%        C
 * 1,210,001~2,420,000   20%        B
 * 2,420,000~4,530,000   30%        A
 * 4,530,001~            40%        S
 */

/**
 * 初階實作
 *
 * 缺點
 * 1. if-else/switch-case述句必須涵蓋所有邏輯
 * 2. 若新增了一個條件，就必須進入函式修改，違反開放封閉原則
 * 3. 重用性差
 */

function calcTax(level, amount) {
  if (level === 'D') {
    return amount * 0.05;
  }

  if (level === 'C') {
    return amount * 0.12;
  }

  if (level === 'B') {
    return amount * 0.2;
  }

  if (level === 'A') {
    return amount * 0.3;
  }

  if (level === 'S') {
    return amount * 0.4;
  }
}

console.log(calcTax('D', 500000));
console.log(calcTax('S', 5000000));

/**
 * 以 策略模式 方式撰寫
 * 1. 定義一系列演算法，封裝起來
 * 2. 讓邏輯可替換
 */

// 定義一系列演算法，封裝起來
const strategyCalcTax = {
  D: function(amount) {
    return amount * 0.05;
  },

  C: function(amount) {
    return amount * 0.12;
  },

  B: function(amount) {
    return amount * 0.2;
  },

  A: function(amount) {
    return amount * 0.3;
  },

  S: function(amount) {
    return amount * 0.4;
  },
};

// 讓邏輯可替換
const personTax = function(level, amount) {
  return strategyCalcTax[level](amount);
};

console.log(personTax('D', 500000));
console.log(personTax('S', 5000000));
