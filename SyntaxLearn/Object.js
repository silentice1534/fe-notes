console.log('--------------------Object.assign()--------------------');
// Object.assign / new object
// 淺拷貝自身物件屬性到另一個物件
// arg1, 目標物件
// arg1, 被拷貝物件

const obj1 = { a: 1, b: 2, c: 3 };
const obj2 = Object.assign({}, obj1);
console.log('obj2: ', obj2); // { a: 1, b: 2, c: 3 }
console.log('obj1 === obj2? ', obj1 === obj2); // false

// 也可以用來合併物件，但目標的物件也會被影響！
const obj3_1 = { a: 1 };
const obj3_2 = { b: 2 };
const obj3_3 = { c: 3 };
const obj3 = Object.assign(obj3_1, obj3_2, obj3_3);
console.log('obj3: ', obj3); // { a: 1, b: 2, c: 3 }
console.log('obj3_1', obj3_1); // { a: 1, b: 2, c: 3 }
console.log('obj3_2', obj3_2); // { b: 2 };
console.log('obj3_3', obj3_3); // { c: 3 };

// 其他注意點:
// 1. 合併的物件中若有相同屬性，後面的屬性會覆蓋前面的屬性
// 2. 原始型別會被包裝成物件
// 3. undefiined 和 null 會被忽略不合併（直接跳過）

console.log('--------------------Object.create()--------------------');
// Object.create() / new object
// 指定一個物件的原型，建立新物件
// arg1, 指定物件原型
// arg2(option), 可以給這個物件預設屬性

// 寫一個簡單的自定義原型
function Obj4(a, b) {
  this.a = a;
  this.b = b;
}

Obj4.prototype.addNumber = function() {
  console.log(this.a, this.b);
  return this.a + this.b;
};

const testObj4 = new Obj4(2, 3);
console.log(testObj4.addNumber());

// 繼承上面自定義的原型鍊，但是原本物件的屬性並不會被繼承
const createCopyObj4 = Object.create(Obj4.prototype);
createCopyObj4.a = 5;
createCopyObj4.b = 6;
console.log(createCopyObj4.addNumber());

// 可以在第二個參數寫這個新物件的屬性，要完整把property descriptors寫進去!，預設是false

const createCopyObj5 = Object.create(Obj4.prototype, {
  a: {
    value: 7,
    writable: true,
    enumerable: true,
    configurable: true,
  },
  b: {
    value: 8,
    writable: true,
    enumerable: true,
    configurable: true,
  },
});

console.log(createCopyObj5.addNumber());

console.log('--------------------Object.defineProperty()--------------------');
// 直接定義物件屬性或修改屬性，會直接呼叫constructor!

const obj5 = {};

Object.defineProperty(obj5, 'prop', {
  value: 100,
  writable: false,
  enumerable: false,
  configurable: false,
});

// enumerable為false，不可枚舉，所以沒有任何console
for (let item in obj5) {
  console.log('item: ', item);
  console.log(obj5[item]);
}

obj5.prop = 120;
console.log(obj5); // writable為false，仍舊輸出100

delete obj5.prop; // configurable為false，屬性不可被改變或刪除
console.log(obj5); // {prop: 100}

console.log(
  '--------------------Object.defineProperties()--------------------',
);
// 可以定義多個新的物件Property descriptor, 或修改現有的屬性的Property descriptor
const obj6 = {};

// 先用 Object.defineProperty 定義屬性
Object.defineProperty(obj6, 'a', {
  value: 100,
  enumerable: true,
  configurable: true,
  writable: true,
});

Object.defineProperty(obj6, 'b', {
  value: 50,
  enumerable: true,
  writable: true,
});

for (let key in obj6) {
  console.log(key, obj6[key]); // a: 100 // b: 50
}

// 現在將a改為不可枚舉，並新增一個c屬性
Object.defineProperties(obj6, {
  a: {
    value: 123,
    enumerable: false,
    configurable: true,
    writable: true,
  },
  c: {
    value: 333,
    enumerable: true,
  },
});

for (let key in obj6) {
  console.log(key, obj6[key]); // b: 50 // c: 333
}

console.log('--------------------Object.freeze()--------------------');
// 凍結物件的Property descriptor
const obj7 = {
  eng: 'abcdefghijk',
};

Object.freeze(obj7);
obj7.eng = 'xyz';
console.log(obj7.eng); // 因為被freeze了，所以仍為 'abcdefghijk'

console.log('--------------------Object.hasOwnProperty()--------------------');
// Object.prototype.hasOwnProperty()
// 回傳是否有某個屬性 / boolean
const obj8 = {
  prop: 'this is prop',
};

console.log(obj8.hasOwnProperty('prop')); // true
console.log(obj8.hasOwnProperty('prop1')); // false

// Object.prototype.isPrototypeOf()
// Object.prototype.propertyIsEnumerable
// Object.prototype.toLocaleString()
// Object.prototype.toString()
// Object.prototype.valueOf()
// Object.setPrototypeOf()

// 重要！!!
// Property descriptor
// 1. value, 屬性值
// 預設 undefined
// 2. writable, 值是否可以被改變
// 預設 false
// 3. enumerable, 此屬性是否可以被枚舉(iterate)
// 預設 false
// 4. configurable, 此屬性是否可以被改變或刪除
// 預設 false
