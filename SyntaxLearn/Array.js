const arr = ['apple', 'banana', 'orange', 'grape'];
// Array.length

console.log('--------------------Array.from()--------------------');
// Array.from() // new array
const txt1 = 'foo';
const arr1 = [1, 2, 3];
// 以下兩個相等
console.log(Array.from(txt1)); // ['f', 'o', 'o']
console.log(txt1.split('')); // ['f', 'o', 'o']

// from 有第二個參數，可以針對解析出來的個數加工

console.log(Array.from(txt1, x => x + x)); // ['ff','oo', 'oo']
console.log(Array.from(arr1, x => x * x)); // [1, 4, 9]

console.log('--------------------Array.from()--------------------');
// Array.isArray() / boolean
// 判斷是否為陣列
console.log(Array.isArray([1, 2, 3])); // true
console.log(Array.isArray({})); // false

console.log('--------------------Array.of()--------------------');
// Array.of() / new array
// 建立一個陣列，不知有啥用 ...
console.log(Array.of(7)); // [7]
console.log(Array(7)); // [empty * 7] 7個空白索引值陣列，這裡的empty是空白，不是undefined!
const arr2 = Array(7);
console.log(arr2[3]);

console.log('--------------------Array.prototype.concat()--------------------');
// Array.prototype.concat() / new array
// 組合兩個陣列
const arr3 = ['a', 'b', 'c'];
const arr4 = ['d', 'e', 'f'];
console.log(arr3.concat(arr4));

console.log(
  '--------------------Array.prototype.copyWithin()--------------------',
);
// Array.prototype.copyWithin() / array
// 將陣列的局部淺拷貝至本身陣列的某個位置，***不會改變本身陣列長度 ！
// arg1, 拷貝目標index
// arg2, 起始拷貝index(含本身)
// arg3, 結束拷貝index(不含本身)
const arr5 = ['a', 'b', 'c', 'd', 'e'];
console.log(arr5);
console.log(arr5.copyWithin(0, 2, 4)); // ['c', 'd', 'c', 'd', 'e'];

console.log(
  '--------------------Array.prototype.entries()--------------------',
);
// Array.prototype.entries() / iterator object
// 返回一個迭代器，可以用來迭代陣列，並每次回傳每個index的[key, value]
const arr6 = ['a', 'b', 'c'];

const iterator1 = arr6.entries(); // 回傳一個迭代器物件
console.log(iterator1.next().value); // [0, 'a']
console.log(iterator1.next().value); // [1, 'b']
console.log(iterator1.next().value); // [2, 'c']
console.log(iterator1.next().value); // 超過陣列個數會回傳 undefined

// 範例:用迭代器迭代陣列key, value
const iterator2 = arr6.entries();
for (let item of iterator2) {
  console.log('item:', item);
}

console.log('--------------------Array.prototype.fill()--------------------');
// Array.prototype.fill() / array
// 在指定區間中填入一個固定的值，會直接在原陣列上操作
// arg1, 靜態值
// arg2, 起始拷貝index(含本身)
// arg3, 結束拷貝index(不含本身)
const arr7 = ['a', 'b', 'c'];
const arr7copy = arr7;
console.log(arr7.fill(1, 1, 10)); // ['a', 1, 1]
// 回傳陣列不會改變長度，就算arg4超過原陣列長度，但不會因此繼續填滿到arg3指定的長度
console.log(arr7.fill({ a: 'aaa', b: 'bbb' }, 0, 2)); // [{ a: 'aaa', b: 'bbb' }, { a: 'aaa', b: 'bbb' }, 1];
console.log(arr7copy); // 因為在原本的陣列上操作，所以參考的變數值也一起改變了

console.log('--------------------Array.prototype.flat()--------------------');
// Array.prototype.flat() / new array
// 將多維陣列轉維一為陣列，預設將二維陣列轉成一維陣列
// arg1, 陣列深度，預設是1, 可以將三維陣列直接轉為一維陣列， 將(n+1)維度轉為一維

const arr8 = [1, 2, [3, 4]];
console.log(arr8.flat()); // [1, 2, 3, 4]

const arr9 = [1, 2, [3, 4, [5, 6]]]; // [1, 2, 3, 4, [5, 6]]
console.log(arr9.flat());

const arr10 = [1, 2, [3, 4, [5, 6]]];
const arr10copy = arr10;
console.log(arr10.flat(2)); // [1, 2, 3, 4, 5, 6]
console.log(arr10copy); // [1, 2, [3, 4, [5, 6]]] 不會跟著改變

// 遇到空陣列時會清除
const arr11 = [1, 2, , , 4, 5];
console.log(arr11.flat()); // [1, 2, 4 ,5]

console.log(
  '--------------------Array.prototype.indexOf()--------------------',
);
// Array.prototype.indexOf() / number
// 搜尋陣列是特定值的索引值
// arg1, 要搜尋的值
// arg2, 從哪個索引值開始,
// arg2 --> 若大於陣列長度不搜尋，回傳 -1
// arg2 --> 若為負數，從陣列尾巴回算索引，但依然從左往右搜尋
// arg2 --> 若負數大於長度，會全部搜尋

console.log('--------------------Array.prototype.join()--------------------');
// Array.prototype.join() / string
// 將陣列轉為文字，預設將每個陣列值以逗號隔開
// arg1 組合的字串的文字
const arr12 = ['apple', 'banana', 'orange'];
console.log(arr12.join()); // 'apple,banana,orange';
console.log(arr12.join('|')); // 'apple|banana|orange';

console.log('--------------------Array.prototype.keys()--------------------');
// Array.prototype.keys() / iterator object
// 回傳一個可以返回陣列中key的迭代器，不會忽略空的索引值，不知有什麼用處 ...
const arr13 = ['a', 'b', 'c'];
const iterator3 = arr13.keys();

for (let key of iterator3) {
  console.log(key); // 0 // 1 // 2
}

console.log(
  '--------------------Array.prototype.lastIndexOf()--------------------',
);
// Array.prototype.lastIndexOf() / number
// 與indexOf()類似，但會回傳最後一個找到的索引值，
// arg1, 要搜尋的值
// arg2, 從哪個索引值開始，由後面往前搜尋
const arr14 = [1, 3, 2, 5, 2, 8, 2, 1];
console.log(arr14.lastIndexOf(2)); // 6
console.log(arr14.lastIndexOf(2, 5)); // 6 會搜尋陣列索引0~5的值，並且是由所以5開始向前

console.log('--------------------Array.prototype.pop()--------------------');
// Array.prototype.pop() / any
// 移除陣列最後一個值，並回傳該值，若為空陣列，回傳undefined
const arr15 = ['grape', 'tomato', 'watermelon'];
console.log(arr15.pop(), '陣列變為:', arr15); //  'watermelon';

console.log('--------------------Array.prototype.push()--------------------');
// Array.prototype.push() / number
// 新增一個值到陣列尾端，並回傳增加後的陣列長度
const arr16 = [1, 2, 3];
console.log(arr16.push(123), '陣列變為:', arr16); // 4

console.log(
  '--------------------Array.prototype.reverse()--------------------',
);
// Array.prototype.reverse()
// 反轉陣列值順序 / array
const arr17 = [1, 2, 3, 4, 5, 6];
const arr17copy = arr17;
console.log(arr17.reverse()); // [6, 5, 4, 3, 2, 1];
console.log(arr17copy);
const arr18 = [1, 2, 3, 4, 5, 6];
console.log(arr18.reverse().reverse()); // [1, 2, 3, 4, 5, 6];

console.log('--------------------Array.prototype.shift()--------------------');
// Array.prototype.shift() / any
// 與pop()相反，移除陣列第一個值並回傳，若為空陣列，回傳undefined
const arr20 = ['grape', 'tomato', 'watermelon'];
console.log(arr20.shift(), '陣列變為:', arr20); // 'grape'

console.log('--------------------Array.prototype.slice()--------------------');
// Array.prototype.slice() // shadow copy new array
// 回傳陣列特定範圍的，返回淺複製的新陣列
// arg1 起始值
// arg2(option) 結束值(不含)
const arr21 = [1, 2, 3, 4, 5, 6];
console.log(arr21.slice(2)); // [3, 4, 5, 6, 7];
console.log(arr21); // [1, 2, 3, 4, 5, 6];
const arr22 = [1, 2, 3, 4, 5, 6, 7];
console.log(arr22.slice(2, 4)); // [3, 4]
console.log(arr22); // [1, 2, 3, 4, 5, 6, 7];

console.log('--------------------Array.prototype.sort()--------------------');
// Array.prototype.sort() / arr
// 預設按照unicode編碼位置排序，數字可能會是錯的 ！
// arg1 比較函式，規則如下
// comparefunction(a, b) 若回傳小於 0, a排在b前面
// comparefunction(a, b) 若回傳等於 0, 排序不變
// comparefunction(a, b) 若回傳大於 0, b排在a前面

const sArr1 = [9, 3, 1, 80, 6, 2];
console.log(sArr1.sort()); // [1, 2, 3, 6, 80, 9]
const sArr2 = ['s', 'b', 'h', 'g', 'l'];
console.log(sArr2.sort()); // ['b', 'g', 'h', 'l', 's']

console.log('--------------------Array.prototype.splice()--------------------');
// Array.prototype.splice() / array
// 在自身陣列刪除特定起始位置一至多個值，並新增新的元素
// *** 回傳被刪除的陣列
// 等於說，自己變成修改後的陣列，回傳的是被刪除的！
// arg1 起始位置
// arg2 刪除個數
// arg3 新增元素
const arr23 = ['a', 'b', 'c', 'd', 'e'];
console.log(arr23.splice(2, 2, 'z')); // ['c', 'd'];
console.log(arr23); //['a', 'b', 'z', 'e'];

console.log(
  '--------------------Array.prototype.unshift()--------------------',
);
// Array.prototype.unshift() / number
// 與shift()顛倒，增加一到多個值到陣列開頭，並回傳增加後的陣列長度
const arr24 = [1, 2, 3];
console.log(arr24.unshift(100)); // 4
const arr25 = [1, 2, 3];
console.log(arr25.unshift(100, 200, 300)); // 6

console.log('--------------------Array.prototype.values()--------------------');
// Array.prototype.values() / iterator object
// 回傳陣列值的迭代器物件，也沒什麼用的感覺
const arr26 = ['w', 'z', 'i', 's', 'a'];
const iterator4 = arr26.values();

console.log(iterator4.next().value); // w
console.log(iterator4.next().value); // z
console.log(iterator4.next().value); // i ... 後面依此類推

const iterator5 = arr26.values();
for (let item of iterator5) {
  console.log(item); // w // z // i // s // a
}

// --------------------------------
// the methods that should pass function

// Array.prototype.forEach()
const arr27 = ['a', 'b', 'c'];
arr27.forEach((item, index) => {});

console.log('--------------------Array.prototype.every()--------------------');
// Array.protottype.every() / boolean
// 回傳給定function條件，全部都符合，回傳true
const arr28 = [1, 2, 3];
const arr29 = [3, 4, 5];

console.log(arr28.every(item => item > 2)); // false
console.log(arr29.every(item => item > 2)); // true

console.log('--------------------Array.prototype.some()--------------------');
// Array.protottype.some() / boolean
// 回傳給定function條件，至少一個符合就回傳true
const arr30 = [1, 2, 3];
const arr31 = [3, 4, 5];

console.log(arr28.some(item => item > 2)); // true
console.log(arr29.some(item => item > 2)); // true

console.log(
  '--------------------Array.prototype.includes()--------------------',
);
// Array.prototype.includes()
// 給定的陣列中是否包含某個值，如果有返回true
const arr32 = ['abc', 'opq', 'xyz'];
console.log(arr32.includes('opp')); // false
console.log(arr32.includes('opq')); // true

// Array.prototype.filter()
// Array.prototype.map()
// Array.prototype.reduce()
// Array.prototype.reduceRight()

console.log('--------------------Array.prototype.find()--------------------');
// Array.prototype.find() / any
// 回傳第一個符合傳入函數的條件的數值
const arr100 = [5, 2, 37, 19, 203, 12];
console.log(arr100.find(item => item >= 19)); // 37

console.log(
  '--------------------Array.prototype.findIndex()--------------------',
);
// Array.prototype.findIndex() / number, first match condition's index
// 回傳第一個符合傳入函數的條件的index
console.log(arr100.findIndex(item => item >= 19)); // 2
