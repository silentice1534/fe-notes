## 前端主要領域

- HTML

  1.  Doctype // v
  2.  行元素、塊元素、空元素 // v
  3.  HTML5 語意化

      3-1. 新增的標籤

      3-2. 廢除的標籤

      3-3. 新增的屬性

  4.  HTML5 新特性
      4-1. Canvas

      4-2. SVG

      4-3. Geolocation

      4-4. WebSocket

      4-5. Drag&Drop

      4-6. Web Workers API

      4-7. Web Storage API

  5.  src 和 href // v

  6.  特殊屬性
      7-1. dataset // v

      7-2. script 中的 async, defer // v

  7.  meta 標籤

- CSS/CSS3

  1. 排版

     1-1. float

     1-2. inline-block

     1-3. position

     1-4. flex

     1-5. grid

     1-6. table

  2. 佈局

     2-1. 三欄

     2-2. 聖杯

     2-3. 雙飛翼

  3. 觀念

     3-1. BFC

     3-2. box-model

     3-3. flex-model

     3-4. inline, inline-block, block

  4. 變形、動畫

     4-1. transform

     4-2. transition

     4-3. animation

  5. 預處理器

  6. 瀏覽器兼容性

  7. 其他

     7-1. pseudo

- JavaScript-Basic

  1.  基本類型、隱式轉換

  2.  傳值/傳參考、shadow Copy, Deep Copy

  3.  上升、TDZ

  4.  作用域、閉包、IIFE

  5.  this

  6.  DOM manipulation

  7.  prototype

  8.  Class

  9.  非同步

  10. RegExp

  11. Strict

  12. setTimeout&setInterval

- JavaScript-Advance

  1. event-loop

  2. currying

  3. web socket

  4. web worker

  5. server worker

  6. PWA, Offline Save

  7. shadow DOM

  8. life cycle

  9. polyfill

- JavaScript-Implement

  1. 手寫防抖

  2. 手寫 new 函式

  3. 手寫 call 函式

  4. 手寫 apply 函式

  5. 手寫 bind 函式

  6. 手寫 instance 函式

  7. deep copy 函式

  8. 手寫 promise 函式

  9. 手寫 currying 函式

  10. 解析 url 查詢參數為物件

  11. 三位一撇

- Browser

  1. 瀏覽器運作

  2. 瀏覽器相容性

- Performence

  1. 資源相關

     1-1. 快取

     1-2. preload & prefetch

     1-3. 文字優化

     1-4. 圖片優化

     1-5. 懶加載

  2. 渲染相關

     2-1. 關鍵轉譯路徑

     2-2. 渲染效能優化

     2-3. CSS 效能優化

     2-4. 動畫效能

  3. 其他

     3-1. JavaScript 效能優化

     3-2. Debounce

     3-3. DNS 效能

     3-4. 效能測試工具

- Security

  1. 權限相關
     1-1. cookie-session

     1-2. token

     1-3. json web token

  2. 密碼相關

     2-1. 非對稱式加密

     2-2. 對稱式加密

  3. 資安相關

     3-1. XSS

     3-2. CSRF

- Network

  1. HTTP

     1-1. 三次握手、四次揮手

     1-1. HTTP/HTTPS

     1-2. HTTP1.0/HTTP1.1/HTTP2.0

     1-4. TCP/IP、UDP

     1-5. 狀態碼

  2. DNS

     2-1. DNS 運作

     2-2. DNS 快取

  3. Web socket

     3-1. web-socket

     3-2. WS/WSS

  4. OSI 七層

- 自動化工具

  1. Webpack

  2. Loader

  3. babel

## 其他

- JavaScript-Vue

- JavaScript-React

- Design Pattern

  1. 單例模式

  2. 策略模式

  3. 裝飾模式

  4. 代理模式

  5. 發布訂閱模式

  6. 觀察者模式

- Algorithm

  1. 時間複雜度、大 O
