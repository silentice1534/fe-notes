// 驗證表單

/**
 * 假設有以下驗證邏輯
 * 1. 欄位不可為空
 * 2. 長度至少 xx個
 * 3. 手機號碼必須符合格式
 * 4. 是否為中文
 * 5. 是否為英文
 * 6. email格式正確嗎
 * 7. 是否超過 xx年
 */

//
const validateErrorMsg = {
  isEmpty: () => 'Input can not be empty',
  stringLength: length => `Input should over ${length} letters`,
  phoneFormat: () => 'Phone number format Incorrect',
  isMamdarin: () => 'Input should be Chinese',
  isEnglish: () => 'Input should be English',
  emailFormat: () => 'Email format is incorrect',
  isOverSomeYear: length => `Year is not over ${length}`,
};

// 封裝若干策略
const formValidateStrategies = {
  isEmpty: function(value, errMsgType) {
    if (value.trim() === '') {
      return validateErrorMsg[errMsgType]();
    }
  },
  stringLength: function(value, length = 6, errMsgType) {
    if (value.trim().length <= length) {
      return validateErrorMsg[errMsgType](length);
    }
  },
  phoneFormat: function(value, errMsgType) {
    const regExp = /^09\d{8}$/;
    if (!regExp.test(value)) {
      return validateErrorMsg[errMsgType]();
    }
  },
  isMamdarin: function(value, errMsgType) {
    const regExp = /[^\u4E00-\u9FA5]/g;
    if (regExp.test(value)) {
      return validateErrorMsg[errMsgType]();
    }
  },
  isEnglish: function(value, errMsgType) {
    const regExp = /[a-zA-Z]/g;
    if (!regExp.test(value)) {
      return validateErrorMsg[errMsgType]();
    }
  },
  emailFormat: function(value, errMsgType) {
    const regExp = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
    if (!regExp.test(value)) {
      return validateErrorMsg[errMsgType]();
    }
  },
  isOverSomeYear: function(value, yearLength = 0, errMsgType) {
    yearLength = Number(yearLength);

    const compareDayArr = value.split('-');
    const compareDayYMD = {
      y: Number(compareDayArr[0]),
      m: Number(compareDayArr[1]),
      d: Number(compareDayArr[2]),
    };

    const today = new Date();
    const todayYMD = {
      y: today.getFullYear(),
      m: today.getMonth() + 1,
      d: today.getDate(),
    };

    if (todayYMD.y - compareDayYMD.y < yearLength) {
      return validateErrorMsg[errMsgType](yearLength);
    }

    if (todayYMD.y - compareDayYMD.y === yearLength) {
      if (
        todayYMD.m - compareDayYMD.m < 0 ||
        todayYMD.d - compareDayYMD.d < 0
      ) {
        return validateErrorMsg[errMsgType](yearLength);
      }
    }
  },
};

// 建立一個驗證物件
const Validator = function() {
  this.cache = [];
};

// 將要驗證的規則寫進去
Validator.prototype.add = function(dom, rule) {
  for (let i = 0; i < rule.length; i++) {
    let argsArr = rule[i].split(':');
    this.cache.push(function() {
      const strategy = argsArr.shift();
      argsArr.unshift(dom.value);
      argsArr.push(strategy);
      return formValidateStrategies[strategy].apply(null, argsArr);
    });
  }
};

// 開始驗證，有錯誤就返回
Validator.prototype.start = function() {
  for (let i = 0; i < this.cache.length; i++) {
    const msg = this.cache[i]();

    if (msg) {
      return msg;
    }
  }
};

// 驗證表單
function validateForm() {
  const validator = new Validator();

  validator.add(form.name, ['isEmpty']);
  validator.add(form.account, ['isEmpty', 'stringLength:8']);
  validator.add(form.phone, ['phoneFormat']);
  validator.add(form.email, ['emailFormat']);
  validator.add(form.mandarin, ['isMamdarin']);
  validator.add(form.english, ['isEnglish']);
  validator.add(form.birthday, ['isOverSomeYear:19']);

  const validateResult = validator.start();
  return validateResult;
}

// 取得DOM與建立Listener
function getElement(element) {
  return document.getElementById(element);
}

function handleSubmit(e) {
  e.preventDefault();

  const errMsg = validateForm();
  if (errMsg) {
    console.log(errMsg);
  } else {
    console.log('pass');
  }
}

const form = getElement('validationForm');
const submitBtn = getElement('submitBtn');
form.addEventListener('submit', handleSubmit);
