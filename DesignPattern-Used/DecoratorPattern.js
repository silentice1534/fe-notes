console.log('======千分位逗號======');

Function.prototype.before = function(fn) {
  const self = this; // 執行環境，before前面的物件

  return function() {
    if (!fn.apply(this, arguments)) {
      return;
    }

    return self.apply(this, arguments);
  };
};

function validateNumber(num) {
  if (typeof num === 'number' && !isNaN(num)) {
    return true;
  }

  return false;
}

// 1. 超暴力解法
function addCommas1(num) {
  return num.toLocaleString();
}

function addCommas2(value) {
  reg = /\d(?=(\d{3})+$)/g;
  const valueStr = String(value);
  if (valueStr.indexOf('.') > -1) {
    const [integer, decimal] = valueStr.split('.');
    return integer.replace(reg, '$&,') + '.' + decimal;
  }

  return valueStr.replace(reg, '$&,');
}

const test1 = 321793812;
const test2 = 321793812.432;
const test3 = -321793812;
const test4 = -321793812.432;

addCommas1 = addCommas1.before(validateNumber);
addCommas2 = addCommas2.before(validateNumber);

console.log(addCommas1(test1));
console.log(addCommas2(test1));
console.log(addCommas1(test2));
console.log(addCommas2(test2));
console.log(addCommas1(test3));
console.log(addCommas2(test3));
console.log(addCommas1(test4));
console.log(addCommas2(test4));
