const typeStrategies = {
  isNumber: function(value) {
    return typeof value === 'number';
  },

  isString: function(value) {
    return typeof value === 'string';
  },

  isBoolean: function(value) {
    return typeof value === 'boolean';
  },

  isSymbol: function(value) {
    return typeof value === 'symbol';
  },

  isUndefined: function(value) {
    return typeof value === 'undefined';
  },

  isNull: function(value) {
    return !value && typeof value !== 'undefined' && value !== 0;
  },

  isFunction: function(value) {
    return typeof value === 'function';
  },

  isArray: function(value) {
    return Array.isArray(value);
  },

  isObject: function(value) {
    return typeof value === 'object';
  },
};

const x = {};
console.log('string', typeStrategies['isString'](x));
console.log('number', typeStrategies['isNumber'](x));
console.log('boolean', typeStrategies['isBoolean'](x));
console.log('symbol', typeStrategies['isSymbol'](x));
console.log('undefined', typeStrategies['isUndefined'](x));
console.log('null', typeStrategies['isNull'](x));
console.log('function', typeStrategies['isFunction'](x));
console.log('array', typeStrategies['isArray'](x));
console.log('object', typeStrategies['isObject'](x));
