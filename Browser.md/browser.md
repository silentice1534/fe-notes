## 常見的瀏覽器內核有哪些

- webkit --> chrome/safari
- gecko --> firebox
- trident --> IE
- presto(以前) --> opera
- blink(現在，webkit 簡化版) --> opera

## 解釋對瀏覽器內核的理解

- 分為兩個部分
  1.  渲染引擎，負責取得網頁內容，html、圖片等，cssom，計算顯示方式
  2.  js 引擎，解析和執行 js

## 解釋 cookie, localStorage 和 sessionStorage 的區別

- cookie
  1.  為了標記使用者身份而存在使用者端的資料
  2.  儲存大小上限 4kb
  3.  cookie 會永遠帶在同源的 http 中，不管需不需要
  4.  可以設定時效
- sessionstorage
  1.  當前頁籤或是瀏覽器關掉就刪除
  2.  儲存大小上限 5mb
- localstorage
  3.  永遠存在使用者端，需手動刪除
  4.  儲存大小上限 5mb

## 瀏覽器如何解析 CSS，選擇器如何運作

_待補_

## 解釋 viewport

_待補_

## 同源策略

- 為了安全性，如果沒有同源策略，http header、cookie、dom、localstorage 就能隨意取用，會天下大亂
- 相同的協議(protocol)、相同的網域(domain)、相同的埠(port)，兩個頁面就屬於同源
- IE 沒有把 port 加入同源策略
- ` <script>``<img>``<iframe>``<link> `中的 src、href 可以任意連接網路資源，並沒有遵守同源策略

## html 的缺點

- iframe 會阻塞 onload 事件
- iframe 和主頁面共享連線池，會影響到瀏覽器的並行載入
- 如果要用 iframe，可以透過 js 動態設定 attribute，可以繞開上面的兩個問題
- 搜尋引擎無法解讀這種頁面，SEO 差
