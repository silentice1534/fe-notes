參考文章 https://juejin.im/post/5af99678f265da0b8e7f881e

只要需要支援到 ie8 就掰掰!!

- 瀏覽器三個主要問題

  1. css 兼容
  2. js 兼容
  3. 瀏覽器 兼容

- 告訴瀏覽器用什麼方式渲染(再多查點資料)

  `<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">`

- CSS3 前綴

  1. -webkit- webkit 引擎 chrome/safari
  2. -moz- gecko 引擎 firefox
  3. -ms- trident 引擎 IE
  4. -o- opeck 引擎 opera

- trasnform, animation 無法實現

  解決方式: 改用 JS

- background-size 不支援 IE

  解決方式: 改用 img

- 用 PIE.htc 讓 IE6/7/8 支援部分 CSS

  1. 部分支援包括`border-radius`, `box-shadow`, `css backgrounds(-pie-background)`, `gradient`, `rgba` 等

  ```
  .border-radius {
    border-radius: 10px;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    background: #abcdef;
    behavior: url(css/PIE.htc);
  }
  ```

- css hack

  1. IE6: `_`
  2. IE7/7: `*`
  3. IE7/Firefox: `!important`
  4. IE7: `*+`
  5. IE6/7/8: `\9`
  6. IE8: `\0`

- IE8 不認識 placeholder

  解決方式: 用 js 插件，如 jquery.JPlaceholder.js

- IE8 不認識`nth-child`, 但認識 `first-child`和`last-child`

  解決方式: 改變寫法，e.g. span:first-child + span 之類的

- 讓瀏覽器認得 html5 標籤，如`<nav>`/`<footer>`

  解決方式: 可以用 html5shiv

- button 預設，IE 是 button，其他默認 submit

  解決方式: 永遠為 button 加上 type 屬性

- firefox 阻止表單默認提交

  解決方式: action="javascript:"

- IE GET 請求過長時報錯

  解決方式: 改用 POST

- 兼容 IE8 new Date()返回 NaN 问题

  解決方式:

  ```
    function parseISO8601(dateStringInRange) {
        var isoExp = /^\s*(\d{4})-(\d\d)-(\d\d)\s*$/,
            date = new Date(NaN), month,
            parts = isoExp.exec(dateStringInRange);

        if(parts) {
            month = +parts[2];
            date.setFullYear(parts[1], month - 1, parts[3]);
            if(month != date.getMonth() + 1) {
                date.setTime(NaN);
            }
        }
        return date;
    }
  ```

- 讓 IE8 兼容 media query

  解決方式: 用 css3-mediaqueries.js

   參考網址: https://www.templatemonster.com/blog/css-media-queries-for-all-devices-and-browsers-including-ie7-and-ie8/

- websocket 如何兼容老瀏覽器

  1. adobe flash socket
  2. ActiveX HTML File(IE) _要研究研究，不知道是什麼_
  3. 基於 multipart 編碼除算 XHR
  4. 長輪詢
