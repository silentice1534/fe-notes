## `<!DOCTYPE html>` 的意義

瀏覽器本身分為兩種模式，標準模式與怪異模式，`<!DOCTYPE>`就是用來觸發瀏覽器使用標準模式，若沒有這個聲明，會進入怪異模式(Quirks Model)。

## HTML4.01 的 Doctype

HTML4.01 基於 SGML，Doctype 聲明引用 DTD，DTD 規定了標記語言的規則。

有三種模式

1. 嚴格模式
   `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">`

2. 過渡模式
   `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">`

3. 框架模式
   `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">`

## HTML5 的 Doctype

HTML5 不基於 SGML，不需要引用 DTD

只有一種模式

`<!DOCTYPE html>`

# 總結

- `<!DOCTYPE>` 用於告訴瀏覽器要用哪一種標準來渲染畫面
