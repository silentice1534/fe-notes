- Doctype 作用，HTML5 為什麼只需要寫 `<!DOCTYPE html>`/`<!DOCTYPE>`

  1. html5 已經不單純是超文字標籤語言，是 html5+
  2. HTML5 不基於 SGML，不需要引用 DTD，但是需要 doctype 規範瀏覽器行為

- 舉例行元素、塊元素、空元素

  1. 行元素 `<span>`, `<a>`
  2. 塊元素 `<div>`, `<nav>`, `<header>`
  3. 空元素 `<br>`, `<hr>`, `<img>`, `<input>`

- 對 HTML 語意化的理解

  1. 每個標籤有適合的使用的場景，例如 label 和 input 的搭配
  2. html5 增強語意化標籤，如`<header>`, `<aside>`, `<footer>`
  3. 用對 html 標籤可以增強 SEO

- html5 有哪些新特性

  1. 增加了許多 API，如 canvas, svg, audio, sessionStorage/localStorage, , geoloacation
  2. XHR level2, websocket
  3. form 增加了許多特性，type 增加了許多值 key, range, tel, email 等，也增加許多屬性，placeholder、autofocus、min、max
  4. 拖放 API

- src 和 href 的區別

  1. 一句話可以說明，src 用於取代該標籤，href 用於與外部的連結
  2. href 不會阻擋網頁載入，src 會等待都載入完後才繼續往下，這可以說明為什麼 script 要放最下面!
  3. @import 會等到頁面載入完成後才載入，這可能導致畫面重新渲染(reflow)而閃爍，所以比較建議用 link!

- _XHTML 有哪些嚴格的規範_

- 對 data-的理解

  `<div data-info="ox">`

  1. js 取得方式
     1-1. 用 `getAttribute('ox')`;
     1-2. 用 dataset `ele.dataset.info`，data-set IE10 以下不支援
  2. css 取得方式
     2-1. 選擇器 `div[data-info='ox']`
     2-2. 偽元素 `article::before {content: attr(data-info)}`
  3. 缺點
     3-1. 不要把需要使用的儲存或使用的內容存到 dataset 裡面，有些網頁無法取得
     3-2. `dataset`速度比`getAttribute`慢

- _standard mode 和 quirk mode 有什麼差別_

- _`<script>`,`<script aysnc>`, `<async defer>` 的差異_

- _用 div 模擬 textarea_
