/**
 * 參考文章
 * https://blog.techbridge.cc/2018/11/10/javascript-hoisting/
 */

/**
 * 1. hoisting
 * 2. hoisting var
 * 3. hoisting var in function
 * 4. hoisting let and const And TDZ
 * 5. TDZ-temporal dead zone
 * 6. why hoisting
 */

// ---------------------------------------------------

/**
 * hoisting
 *
 * 1. 變數宣告與函式宣告都會有hoisting
 * 2. 只有宣告會hoisting，賦值不會hoisting
 *
 * 宣告 var x = 10;
 * 這其實是兩個步驟
 * 執行順序如下
 * var x; // x --> undefined
 * x = 10; // x --> 10
 */

// ---------------------------------------------------

/**
 * hoisting var
 */

// xxx is not defined --> xxx 不存在，記憶體上沒有這個東西
// undefined ---> 記憶體上存在，但裡面是空的

// e.g 1
// console.log(a); // ReferenceError: a is not defined

// // e.g 2
// console.log(b); // undefined
// var b;

/**
 * 可以將e.g 2.想像成
 *
 * var b;
 * console.log(b);
 *
 * 由 e.g 1, e.g 2 可以發現b被hoisting了
 */

// ---------------------------------------------------

/**
 * hoisting var in function
 */

// e.g 3
function h(x) {
  var x;
  console.log(x); // 10
  var x = 3;
  console.log(x); // 3
}

h(10);

/**
 * 這裡可以想像成
 * function h(x) {
 *   var x = 10;
 *   var x;
 *   console.log(x); // 10
 *   var x = 3;
 *   console.log(x); // 3
 * }
 *
 * h(10);
 *
 * 在function內部，遵循hoisting規則，1.先上升 2.後賦值，實際執行時如下
 * var x; --> hositing; (對應var x = 10;)
 * var x; --> hositing; (對應var x;)
 * var x; --> hositing; (對應var x = 3;)
 * x = 10; --> 10; // 第一次console.log --> 10
 * x = 3; --> 3; // 第二次console.log --> 3
 *
 */

// e.g 4
console.log(f); // f a() {}
var f;
function f() {}

// 函式的優先權更高

/**
 * hoisting let and const And TDZ
 *
 * let 和 const 的 hoisting 行為是一樣的，下面以let為例
 * var 和 let 與 const 行為不一樣
 */

// e.g 5
// console.log(c1); // undefined
// console.log(c2); // Uncaught ReferenceError: Cannot access 'c' before initialization
// var c1;
// let c2;

/**
 * 不可以在用 let 或 const 宣告前就使用
 * 這不代表 let 和 const 沒有 hoisting !!!
 */

// !!!
// e.g 6 // ---> 這有些小問題，跟參考網站給的結果不同，再釐清一下
// var d = 10;
// function lc() {
//   console.log(d); // 原本根據作用域d應該是10，但這邊會拋出錯誤，因為在TDZ中(看下面解釋)
//   let d;
// }

// lc();

/**
 * TDZ-temporal dead zone
 *
 * 1. TDZ指的是用let或cosnt宣告時(一開始只宣告了變數尚未賦值)，到該變數被賦值期間稱為TDZ，也就是在「上升之後」到「賦值之前」，這是專指let和const這個現象的名詞
 *
 * 2. 若在TDZ之前使用該變數，會拋出錯誤
 * 3. var 沒有TDZ 可以從 e.g.5看出差異，var在宣告後賦值之前會被初始化為undefined
 * 4. 可以總結一句話，let和const也有hoisting
 */

// e.g. 7

function eg7() {
  yo(); // c 的 TDZ 開始，因為會上升
  let c = 10; // c 的 TDZ 結束
  function yo() {
    console.log(c);
  }
}
eg7();

/**
 * why hoisting
 */

/**
 * 若沒有hoisting會有以下三種情況
 * 1. 先宣告變數才可以使用，這沒問題
 * 2. 先宣告函式才可以使用，
 * 3. 函式無法互相呼叫，因為不能同時a再b上面 b又在a下面
 */
