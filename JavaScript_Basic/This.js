/**
 * 參考網站
 * https://wcc723.github.io/javascript/2017/12/12/javascript-this/
 * https://kuro.tw/posts/2017/10/12/What-is-THIS-in-JavaScript-%E4%B8%8A/
 * https://cythilya.github.io/2018/10/23/this/
 */

/**
 * this是函式執行時的一個物件，值會根據執行環境的不同而有所不同，大致可歸類下面幾種
 *
 * 1.預設綁定-不式2.3.4.的情況時
 * 2.隱含綁定-在物件的方法中時
 * 3.明確綁定-使用call(), apply(), bind()時
 * 4.new綁定-使用new建構子時
 * 5.例外
 *
 */

console.log(typeof this);
// object

// ---------------------------------------------------

/**
 * 預設綁定
 *
 * 不在物件方法中，沒有使用call(), apply(), bind()時, 沒有用new建構子時，
 * this就是預設全域物件，在瀏覽器的環境下就是Window
 */

var sayHello = 'hello';
const sayHi = 'hi';

function hi() {
  console.log(this); // Window
  console.log(this.sayHello); // hello
  console.log(this.sayHi); // undefined
}

hi();

// 若在函式中使用嚴格模式，this會變成undefined

function strict() {
  'use strict';
  console.log(this); // undefined
}

strict();

// ---------------------------------------------------

/**
 * 隱含綁定
 *
 * 當函式為物件的方法時，執行階段this會被綁定到這個物件上
 */

const obj = {
  name: 'Andy',
  show: function() {
    console.log(this.name); // Andy
  },
  showArrow: () => {
    console.log(this); // Window
  },
};

console.log(obj.show());

// 此處不可使用箭頭函式，因為箭頭函式沒有自己的this，會指向Window
console.log(obj.showArrow());

/**
 * 隱含的失去
 * 失去綁定的物件，退回到預設綁定的window
 *
 * 1.函式是另一個函式的參考
 * 2.參數傳遞中的callback
 * 3.DOM Element 事件綁定
 */

// 函式是另一個函式的參考
function refLost() {
  console.log(this.a);
}

const refObj = {
  a: 'a in obj',
  refLost: refLost,
};

var a = 'a in global';
var runRef = refObj.refLost;
const cRef = refObj.refLost;

console.log(refObj.refLost()); // --> a in obj
console.log(runRef()); // --> a in global
console.log(cRef()); // --> a in global

// 參數傳遞中的callback
function paramFunc() {
  console.log(this.b);
}

function runFunc(fn) {
  fn();
}

var b = 'b in global';

const paramObj = {
  b: 'b in obj',
  func: paramFunc,
};

console.log(runFunc(paramObj.func)); // --> b in global

// DOM Element 事件綁定
const dom = document.getElementById('btn');

dom.addEventListener('click', function() {
  console.log(this); // <button id="btn">btn</button>
});

// ---------------------------------------------------

/**
 * 明確綁定
 *
 * call(), apply(), bind()
 */

// call()
// 第一個參數放this, 後面就是參數
const animalDog = {
  type: 'dog',
};

const animalCat = {
  type: 'cat',
};

function showAnimal(amount) {
  console.log(this.type);
  console.log(amount);
}

console.log(showAnimal.call(animalDog, 3)); // --> dog, 3
console.log(showAnimal.call(animalCat, 5)); // --> cat, 5

// apply()
// 與call的差別只有傳參數的方式，需要用陣列
console.log(showAnimal.apply(animalDog, [9])); // --> dog, 9
console.log(showAnimal.apply(animalCat, [8])); // --> cat, 8

// call()和apply()可以看作是執行function的另一種方式，明確綁定了this並在執行時使用

// bind()
// 與call()和apply()不同在於，bind()會在return這個function，並在執行前就綁定上去了!
// 所以需要再另外呼叫一次
console.log(showAnimal.bind(animalDog)(10)); // --> dog, 10
console.log(showAnimal.bind(animalCat).call(null, 11)); // --> cat, 11

// bind是硬綁定，即便後面用call(), apply()重新指定其他this，也不會改變內部的this
console.log(showAnimal.bind(animalDog).call(animalCat, 53)); // --> dog, 53
console.log(showAnimal.bind(animalDog).apply(animalCat, [55])); // --> dog, 53

// setTimeout, setInterval可以用bind將this帶入自身的callback
// 看上面隱含的失去的第二點，因為setTimeout, setInterval中的function是callback，
// 且被當參數傳遞，所以this會退回預設綁定變成Window

var timeObj = {
  time: new Date(),
};

setTimeout(function() {
  console.log(time); // ReferenceError: time is not defined
  console.log(this.time); // undefined
}, 1000);

setTimeout(
  function() {
    console.log(this.time); // new Date()的時間
  }.bind(timeObj),
  1000,
);

// ---------------------------------------------------

/**
 * new綁定
 *
 * this會指向new出來的物件
 */

function thisFunc(name, age) {
  this.name = name;
  this.age = age;
  this.func = function() {
    console.log(this.name + ' is ' + this.age + ' years old.');
  };
}

const p = new thisFunc('Allen', 9);
console.log(p.name); // Allen
console.log(p.func()); // --> Allen is 9 years old.

/**
 * 例外
 *
 * 間接參考
 * 還有一些看參考的第三個網址
 */

function showC() {
  console.log(this);
  console.log(this.c);
}

var c = 'Kevin';

var otherObj = {
  c: 'Puppy',
  showC: showC,
};

var refOtherObj = {
  c: 'Kitty',
};

otherObj.showC(); // --> puppy, 23
(refOtherObj.showC = otherObj.showC)(); // --> window, Kevin
// is ***not*** same as ...
// refOtherObj.showC = otherObj.showC;
// refOtherObj.showC();
