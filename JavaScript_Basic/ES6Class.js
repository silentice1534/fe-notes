/**
 * 參考網站
 *
 * http://shubo.io/javascript-class/
 * https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Classes/static
 * https://cythilya.github.io/2018/10/28/es6-class/
 *
 * 1. class 語法
 * 2. 靜態方法 static
 * 3. 繼承 extends
 * 4. 利用 super 覆寫 母類別方法
 * 5. 利用 super 覆寫 constructor
 * 6. super 在原本物件中的使用
 */

/**
 * class語法
 *
 * class語法並不是真正的物件導向中的類，只是一個語法糖！
 */

// 傳統要利用建構函式會需要寫一個constructor，在prototype定義方法
function Person(name) {
  this.name = name;
}

Person.prototype.showName = function() {
  console.log('Name: ', this.name);
};

const student = new Person('John');

console.log(student.showName()); // Name: John

// 上面的例子改用class寫法
class Person2 {
  constructor(name) {
    this.name = name;
  }

  showName() {
    console.log('Name: ', this.name);
  }
}

const student2 = new Person('Wendy');

console.log(student2.showName()); // Name: Wendy

/**
 * class 做了以下幾件事
 *
 * 1. 把constrcutor中的屬性指定給class（例子中的Person2)
 * 2. 把其他的方法指定給class(例子中的Person2)的prototype
 */

// --------------------------------------------------

/**
 * 靜態方法 static
 *
 * 方法是直接定義在類上，就跟寫一般primitive object一樣，不需要實例化
 *
 */

class Animal {
  constructor(c) {
    this.c = c;
  }

  static showC(c) {
    // --> 直接定義在這個類上
    return 'c is: ' + c;
  }
}

console.log(Animal.showC('cat')); // c is: cat

// --------------------------------------------------

/**
 * 繼承 extends
 */

class Car {
  constructor(name) {
    this.name = name;
  }
  move() {
    console.log('move move move');
  }
}

class SuperCar extends Car {}

const superCar = new SuperCar();
console.log(superCar.move()); // 'move move move',

/**
 * 把Supercar的原型鍊指到Car.prototype上
 * SuperCar.prototype.__proto__ --> Car.prototype
 *
 * 使用extends繼承時連靜態方法都會繼承！extends會做兩件事
 * 1. child.prototype.__proto__ === Parent.prototype
 * 2. child..__proto__ === Parent
 *
 */

// --------------------------------------------------

/**
 * 利用 super 覆寫 母類別方法
 */

// 沿用上面的例子
class SuperCar2 extends Car {
  move() {
    super.move();
    console.log('move faster~~~~');
  }
}

const superCar2 = new SuperCar2();
console.log(superCar2.move()); // 'move move move', 'move faster~~~~'

// --------------------------------------------------

/**
 * 利用 super 覆寫 constructor
 */

// 沿用上面的例子
class SuperCar3 extends Car {
  constructor(name, wheelCount) {
    super(name);
    this.wheelCount = wheelCount;
  }
}

const superCar3 = new SuperCar3('Puppy', 8);
console.log(superCar3);

/**
 * 注意點
 * 1. super 中的屬性必須先在母類別定義，不然相當於沒者個屬性
 * 2. super 一定要寫在consrtuctor中的的最上面，不然會跳error
 *    因為預期我們用super就是要繼承母類別中的屬性，
 *    如果super寫在子constrcutor其他屬性的下面，有可能會被子類別的屬性覆蓋
 * 3. 如果子類別中需要建立新的屬性，上面也要先寫一個空的super
 *    因為會將this指向物件這個動作只有在母類別中發生，子類別不會做第二遍
 */

// --------------------------------------------------

/**
 * super 在原本物件中的使用
 */

const obj = {
  name: 'Steve',
  showName: function() {
    console.log(this.name);
  },
};

const shortHandObj = {
  name: 'Kevin',
  showName() {
    console.log(this.name);
  },
};

console.log(obj.showName()); // --> Steve
console.log(shortHandObj.showName()); // --> Kevin

const childObj = {
  __proto__: obj,
  showName: function() {
    // super.showName(); --> 這行會出錯!!!
    console.log('~~~');
  },
};

const childShortHandObj = {
  __proto__: shortHandObj,
  showName() {
    super.showName();
    console.log('~~~');
  },
};

console.log(childShortHandObj.showName());

/**
 * 原本使用 showName 是一個"屬性"
 *  showName: function() {
 *   // super.showName(); --> 這行會出錯!!!
 *   console.log('~~~');
 * },
 *
 * 新的寫法 是一個"物件方法"
 *
 * showName() {
 *    super.showName();
 *    console.log(this.name);
 * },
 *
 * 而super不能用在這屬性的是function中
 * 新的寫法中多了一個隱藏的 [[HomeObject]] 屬性，所以super才可以正確執行
 */
