/**
 * 參考網站
 * https://ithelp.ithome.com.tw/articles/10192739
 * https://wcc723.github.io/life/2017/05/25/promise/
 * https://dotblogs.com.tw/wasichris/2017/08/15/021114
 * https://medium.com/@peterchang_82818/javascript-es7-async-await-%E6%95%99%E5%AD%B8-703473854f29-tutorial-example-703473854f29
 *
 * 1. callback function
 * 2. Promise
 * 3. Promise.all()
 * 4. Async/Await
 */

/**
 * callback function
 *
 * callback與一般個function沒有差別，而差別在於呼叫的時間點
 * 具體一點來說，callback需要滿足某個條件才會被呼叫
 */

// e.g. event // 滿足條件:被點擊時
const btn = document.getElementById('btn');
function fn() {
  console.log('clicked !');
}
btn.addEventListener('click', fn);

// e.g. setTimout // 滿足條件:1秒鐘後
setTimeout(() => console.log('call after 1 sec'), 1000);

// e.g. 確保某個順序
function a() {
  const random = Math.random() + 1;
  setTimeout(() => console.log('a function'), random * 1000);
}

function b() {
  const random = Math.random() + 1;
  setTimeout(() => console.log('b function'), random * 1000);
}

a();
b();

// 這時候就不知道誰會先執行，所以依賴js的耽執行緒，可以確保等待A事情完成後再執行B事情

function doFirst(fn) {
  console.log('do first');
  const random = Math.random() + 1;

  setTimeout(() => fn(), random * 1000);
}

function doSecond() {
  console.log('do second');
}

doFirst(doSecond);

// 這樣就可以確定在A執行完後再執行B

function a1() {
  setTimeout(() => console.log('a1'), 1000);
}

function b1() {
  console.log('b1');
}

function do1(a1, b1) {
  a1();
  b1();
}

do1(a1, b1);

/**
 * promise
 *
 * Promise物件中的函式只是某間非同步事情，當非同步執行完會執行resolve或reject
 * 這裡的
 */

function doAsync() {
  return new Promise((resolve, reject) => {
    // 這個function 就是讓你做一些非同步的事情而已 !!
    // 結果出來之後要做什麼事情，由下面接續的 then()/catch 執行
    // 做一些非同步的事情
    const isSuccess = true;

    setTimeout(() => {
      if (isSuccess) {
        resolve(); // 這個resolve 對應到下面的 succ()
      } else {
        reject(); // 這個reject 對應到下面的 fail()
      }
    }, 3000);
  });
}

function succ() {
  console.log('is succes');
  return 'hello a';
}

function fail() {
  console.log('is fail');
  return 'hello false';
}

doAsync()
  .then(succ)
  .then(a => console.log(a))
  .catch(fail);

// is same as ...

// doAsync()
//   .then(succ, fail)
//   .then(a => console.log(a));

/**
 * then 執行函式的回傳值會等於下一個then傳送的變數
 */

/**
 * Promise.all
 *
 * 是一個靜態方法, 參數放一個promise的陣列
 * 當所有方法都成功時執行 resolve
 */
const promise1 = new Promise(resolve => {
  setTimeout(() => {
    console.log('promise1 is success');
    resolve();
  }, 2000);
});

const promise2 = new Promise(resolve => {
  setTimeout(() => {
    console.log('promise2 is success');
    resolve();
  }, 3000);
});

const promise3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    console.log('promise3 is false');
    resolve();
  }, 2000);
});

Promise.all([promise1, promise2, promise3])
  .then(() => console.log('promise1 and promise2 are success'))
  .catch(() => 'something error');

/**
 * 當遇到失敗時，會直接跳到catch, then就不執行了！
 * 但是原本各自的Promise還是會執行完
 *
 */

/**
 * promise.race()
 *
 * 是一個靜態方法, 參數放一個promise的陣列
 * 回傳最快完成的
 */

const promiseRace1 = new Promise(resolve => {
  setTimeout(() => {
    resolve('promiseRace1');
  }, 1234);
});

const promiseRace2 = new Promise(resolve => {
  setTimeout(() => {
    resolve('promiseRace2');
  }, 999);
});

const promiseRace3 = new Promise(resolve => {
  setTimeout(() => {
    resolve('promiseRace3');
  }, 1232);
});

Promise.race([promiseRace1, promiseRace2, promiseRace2]).then(result =>
  console.log(result),
);

/**
 * async await
 */

function waitA() {
  setTimeout(() => console.log('wait A'), 2000);
}

async function asyncFunc() {
  await waitA();
  console.log('after fulfill');
}

asyncFunc();
