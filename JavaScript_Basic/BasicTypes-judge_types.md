# How To Know The Type

1. typeof
2. instanceof
3. constructor
4. Object.prototype.toString.call()

## typeof

`typeof ''` // string

`typeof 0` // number

`typeof true` // boolean

`typeof undefined` // undefined

`typeof null` // **object**

`typeof new RegExp()` // object

`typeof []` // object

`typeof new Function()` // **function**

`typeof new Date()` // object

`typeof new RegExp()` // object

- 基本類型除了 null，返回的都是正確的
- 參照類型除了 function，都返回 object
- **null 會返回 object**
- **function 會返回 function**

null 應該返回 null，而參照類型只返回了物件的頂層(object)，所以 typeof 還無法判斷所有類別情況。

## instanceof

判斷 A 是否為 B 的實例，instanceof 判斷的是原型，會指到原型鏈最上層

`[] instanceof Array` // true

`{} instanceof Object` // true

`new Date() instanceof Date` // true

## constructor

https://www.cnblogs.com/onepixel/p/5126046.html

(... 待補)

## toString

toString() 是 Object 的原型方法，默認返回該物件的 Class。這是一個內部屬性，格式為[object xxx]，xxx 就是類型。

對於 Object 物件直接調用 toStriig()就可以返回[object Object]

e.g. `const obj = {}; obj.toString()` // [object Object]

`Object.prototype.toString.call('');` // [object String

`Object.prototype.toString.call(1);` // [object Number

`Object.prototype.toString.call(true) ;` // [object Boolean

`Object.prototype.toString.call(Symbol());` //[object Symbol

`Object.prototype.toString.call(undefined)` ; // [object Undefined

`Object.prototype.toString.call(null);` // [object Null

`Object.prototype.toString.call(new Function()) ;` // [object Function

`Object.prototype.toString.call(new Date()) ;` // [object Date

`Object.prototype.toString.call([]) ;` // [object Array

`Object.prototype.toString.call(new RegExp()) ;` // [object RegExp

`Object.prototype.toString.call(new Error()) ;` // [object Error

`Object.prototype.toString.call(document) ;` // [object HTMLDocument

`Object.prototype.toString.call(window) ;` //[object Window] window 全域物件 global 的引用
