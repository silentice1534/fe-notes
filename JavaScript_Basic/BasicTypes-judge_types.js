/**
 * JS在es5中有五大基本型別
 *
 * 1. Number
 * 2. String
 * 3. Boolean
 * 4. undefined
 * 5. null
 *
 * 在es6中新增了第六個型別
 *
 * 6. symbol
 */

/**
 * 不是基本型別的都是物件型別
 *
 * 1. function
 * 2. array
 * 3. new 出來的東西 e.g. RegExp(), Date()
 * 4. arguments(在function中出現的類陣列)
 *
 * js內建的物件型別有 Date(), RegExp(),
 */

/**
 * JS 的型別非常亂，常常出現不預期的型別
 *
 * 主要有四種判斷型別的方式
 * 1. typeof // 判斷基本型別，但有null === object/ new Function() === function的例外
 * 2. instanceof // 判斷是不是與某個類型原型鏈有關
 * 3. Object.prototype.toString.call()
 *
 * 其他
 * 4. Array.isArray()
 */

/**
 * 1. typeof
 * typeof [variable] --> variable的類型是 ?
 * 返回如右七種型別 number、boolean、symbol、string、object、undefined、function
 *
 * 對開發者來說，typeof可以判斷是基本型別與或物件型別，但無法知道物件型別的引伸型別為何。
 * 除此之外，typeof判斷出某些數值會出現不預期的型別。
 */

// 基礎型別
console.log(typeof ''); // string
console.log(typeof 0); // number
console.log(typeof true); // boolean
console.log(typeof undefined); // undefined
console.log(typeof null); // **object**
console.log(typeof Symbol()); // symbol

// 基礎型別
console.log(typeof {}); // object
console.log(typeof []); // object
console.log(typeof new RegExp()); // object
console.log(typeof new Function()); // function
// is same as ...
console.log(typeof function foo() {}); // function

/**
 * 例外一
 * null返回object，
 * 這是一個bug，但更改這項bug會造成許多網站掛掉，所以就不修了 ...
 *
 * 例外二
 * Function() 會返回function
 */

// --------------------------------------------------

/**
 * 2. instanceof
 *
 * 語法
 * [variable] instanceof [type] --> variable是不是type類型
 * 返回 true/false
 *
 * instanceof 檢測的是原型，只要 ooo.__proto__ 指向 xxx.prototype，即為true
 */

console.log([] instanceof Array); //true
console.log([] instanceof Object); //true
console.log(new Date() instanceof Date); // true

/**
 * 以上三個例子說明instanceof檢查的是原型
 *
 * []在Array的直接在原型鏈下，也間接在Object原型鏈下
 * [].__proto__ --> Array.prototype --> Object.prototype --> null
 *
 * 由此可知，instanceof只能判斷的和原型的關係是不是成立，而不能直接判斷是不是某個類型
 */

// --------------------------------------------------

/**
 * 3. Object.prototype.toString.call([variable])
 * toString是Object的prototype方法
 * 返回[class]值，e.g. [object xxx]，xxx是具體對應的類型
 */

// 對於object物件，可以直接調用，因為在自己的原型鏈上
const o = {};
console.log(o.toString()); // [object Object]

console.log(Object.prototype.toString.call('')); // [object String]
console.log(Object.prototype.toString.call(1)); // [object Number]
console.log(Object.prototype.toString.call(true)); // [object Boolean]
console.log(Object.prototype.toString.call(Symbol())); //[object Symbol]
console.log(Object.prototype.toString.call(undefined)); // [object Undefined]
console.log(Object.prototype.toString.call(null)); // [object Null]
console.log(Object.prototype.toString.call(new Function())); // [object Function]
console.log(Object.prototype.toString.call(new Date())); // [object Date]
console.log(Object.prototype.toString.call(new RegExp())); // [object RegExp]
console.log(Object.prototype.toString.call(new Error())); // [object Error]
console.log(Object.prototype.toString.call([])); // [object Array]
console.log(Object.prototype.toString.call(document)); // [object HTMLDocument]
console.log(Object.prototype.toString.call(window)); //[object Window] window 是全局对象 global 的引用

// --------------------------------------------------

/**
 * 4. Array.isArray()
 *
 * 用來檢查某個變數是不是陣列
 * 檢查的是[class]值，e.g. [object xxx]，xxx是具體對應的類型
 */

console.log(Array.isArray([1, 2, 3])); // true
console.log(Array.isArray({})); // false

// --------------------------------------------------

// 判斷類型
function judgeType(value) {
  // 是否有參數
  if (arguments.length == 0) {
    return 'no argument';
  }

  // 基本類型
  if (typeof value === 'number') {
    return 'number';
  }
  if (typeof value === 'string') {
    return 'string';
  }
  if (typeof value === 'boolean') {
    return 'boolean';
  }
  if (typeof value === 'symbol') {
    return 'symbol';
  }
  if (value === undefined && arguments.length > 0) {
    return 'undefined';
  }
  if (!value && typeof value !== 'undefined' && value !== 0) {
    return null;
  }

  // 物件類型
  if (value instanceof Function || typeof value === 'function') {
    return 'Object function';
  }
  if (Array.isArray(value)) {
    return 'Object array';
  }

  // 可以用來判斷 array, Date(), RegExp() ...
  if (typeof value === 'object') {
    const actualType = Object.prototype.toString.call(value);

    return `Object ${actualType}`;
  }
}
