const obj = {
  sayHi: 'hello',
  sayBye: 'ByeBye',
};

const fn = function(arg1, arg2) {
  console.log(this);
  console.log(arg1);
  console.log(arg2);
  console.log(arguments.length);
};

fn.apply(obj, [1, 2]);

const list = [1, 5, 3];

function calcTotal() {
  console.log(arguments.__proto__);
  console.log(Object.prototype.toString.call(arguments));
  return Array.from(arguments).reduce((sum, value) => sum + value, (sum = 0));
}

console.log(calcTotal.apply(null, list));

// bind 的神奇之處！
function add(a, b) {
  console.log(a + b);
}

console.log(add.call(null, 3, 2)); // 5
console.log(add.apply(null, [7, 1])); // 8
const newAdd = add.bind(this, 1);
console.log(newAdd(2)); // 3
console.log(newAdd(11)); // 12

const person = {
  name: 'Jacky',
  age: 25,
};

function callFn(arg1) {
  console.log(arg1, this.name);
}

console.log(callFn('noCall'));
console.log(callFn.call(person, 'withCall'));
