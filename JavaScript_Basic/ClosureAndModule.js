/**
 * 參考網站
 *
 * https://cythilya.github.io/2018/10/22/closure/
 *
 * 1. 閉包
 * 2. ES6 Module
 */

/**
 * 閉包式函式存取語彙範疇的能力，因此當韓式離開其宣告的語彙範疇也能正常運作
 *
 * 能夠存取的範圍是宣告在地方依範圍鍊向外的範圍，包括參數。
 * 即可以存取外層函式的函式
 */
import b from './ClosureAndModule_test.js';

const a = '10';

// e.g.
function closure(b) {
  const a = '123';

  return function() {
    console.log(a);
    console.log(b);
  };
}

const execClosure = closure(10);
console.log(execClosure()); // --> 123 , 10

// 最常見的setTimeout之類的就不說了 ~~~

/**
 * ES6 Module
 *
 * 通常講到閉包都是在說範圍鏈與和命名衝突之類的問題。
 * 可以通過ES6 Module當作其中一種解法
 */

/**
 * 在用<script></script>想使用模組時 不管是export還是import都需要加上 type="module"
 *
 * e.g. <script type="module" src="..."></script>
 *
 * 小記...第一次使用就是在框架，用最基本方式竟然被雷到 !!!
 */
