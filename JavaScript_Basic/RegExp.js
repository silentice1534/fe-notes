/**
 * 建立正規表達式的兩種方式
 */

// 1. 用字面值建立，效能較佳
const re1 = /abc/;
// 2. 用RegExp物件建立
const re2 = new RegExp('ab+c');

/**
 * 兩種語法
 *
 * 1. test()判斷是否符合，返回true/false [regExp].test(text)
 * 2. exec()執行一次比對並反回一個陣列 [regExp].exec(text)
 * 3. match() text.match([regExp])
 */

// exec()
const reg1 = /abc/;
const text1 = 'fdsafsabcfadsf';
const match = reg1.exec(text1);
console.log(match);
// ["abc", index: 6, input: '', groups: undefined] --> 返回regExp, 第一個匹配的索引, 測驗的數字
console.log(match[0]); // abc
console.log(match.index); // 6
console.log(match.input); // fdsafsabcfadsf
console.log(match.group); // undefined

// match
const reg2 = /xyz/;
const text2 = 'fdafsxyzdfaag';
const result2 = text2.match(reg2);
// 返回結果與exec()相同

/**
 * 正規表達式的參數
 * 1. g (stand of global)
 * 尋找所有匹配，不會再找到第一個後即停止
 * 2. i (stand of insensitive)
 * 忽略大小寫
 * 3. m (stand of multi)
 * 會匹配每一行的結果，如果一個字串被\b或\r分割，^和$會被視為在每一行上工作
 * 4. u
 * Unicode，將模式視為Unicode序列點的序列 (待釐清...)
 * 5. y
 * (待釐清...)
 */

/**
 * 字符相關
 * 1. .
 * 2. \d
 * 3. \D
 * 4. \w
 * 5. \W
 * 6. \s
 * 7. \S
 * 8. \t
 * 9. \r
 * 10. \n
 * 11. \v
 * 12. \f
 * 13. [\b]
 * 14. \0
 * 15. \cX
 * 16. \xhh
 * 17. \uhhhh
 * 18. \
 */

// . 匹配任意單一字符，至少要代表一個字符，不可為無，但\t(tab) \r(enter) 例外
const stringRE1 = /.y/;
console.log(stringRE1.test('yes')); // false
console.log(stringRE1.test('day')); // true --> 'day'
console.log(stringRE1.test('y')); // false

// \d 匹配任意阿拉伯數字，相等於[0-9]

// \D 匹配任意非阿拉伯數字，相等於[^0-9]，這裡的^是反義運算元

// \w 匹配任意英文大小寫、數字和底線，相等於[a-zA-Z0-9_]

// \W 匹配任意非英文大小寫、數字和底線，相等於[a-zA-Z0-9_]，這裡的^是反義運算元

// \s 匹配空白符號，包括 \f, \n, \r, \t, \v與對應unicode碼

// \S 匹配非空白符號，包括 \f, \n, \r, \t, \v與對應unicode碼

// \t tab

// \r enter

// \n 換行

// \v 垂直tab

// \f 換頁符號

// [\b] 退格符號，與\b不同

// \0 nul符號

// \cX X是A-Z的其中一個字母 (待釐清 ...)

// \xhh 匹配兩個十六至近位數字的字符

// \xhhhh 匹配兩個十六至近位數字的字符

// \b 代表邊界，前後沒有字元/字串
const stringRE2 = /\btest\b/;
console.log(stringRE2.test('fdstestfdas')); // false
console.log(stringRE2.test('fds test fdas')); // true

/**
 * 字符集合
 * 1. [xyz]
 * 2. [^xyz]
 */

// [xyz]，匹配[]中任意字元即為符合
// 可以用-指定一個範圍，例如 [a-b] 相等於 [abcd]

// [＾xyz]，不匹配[]中任意字元即為符合，^為反義字元
// 可以用-指定一個範圍，例如 [a-b] 相等於 [abcd]

/**
 * 邊界
 *
 * 1.^
 * 2.$
 */

// ^ 字串起始，若設定多行，每一行第一個字也會被匹配

// $ 字串起始，若設定多行，每一行第一個字也會被匹配

/**
 * 數量相關
 * 1. x*
 * 2. x+
 * 3. x*? (待釐清...)
 * 4. x+? (待釐清...)
 * 5. x?
 * 6. x(?=y)
 * 7. x(?|y)
 * 8. x|y
 * 9. x{n}
 * 10. x{n,}
 * 11. x{n,m}
 */

// * 匹配0次或多次
const cRE1 = /bo*/;
console.log(cRE1.test('ffd boooed')); // true  --> 'booo'
console.log(cRE1.test('bird')); // true  ---> 'b'
console.log(cRE1.test('fgasg')); // false

// + 匹配一次或多次，功能與 {1,}相等
const cRE2 = /a+/;
console.log(cRE2.test('candy')); // true ---> 'a'
console.log(cRE2.test('caaaaandy')); // true ---> 'aaaaa'

// x? 匹配前面符號0或1次

// x(?=y) 只有在x後面匹配y時才符合
const cRE3 = /hello(?=_world)/;
console.log(cRE3.test('hello world')); // false
console.log(cRE3.test('hello_world')); // true
const cRE4 = /hello(?=_world|_monkey)/;
console.log(cRE4.test('hello_monkey')); // true

// x(?!y) 只有在x後面不是匹配y時才符合
const cRE5 = /foo(?!_hi)/;
console.log(cRE5.test('foo_hi')); // false
console.log(cRE5.test('foo_hello')); // true

// x|y 匹配x或y其中一個

// x{n} x連續出現n符合

// x{n,} x連續出現至少n次符合

// x{n,m} x連續出現至少n次，至多m次符合

// --------------------------------------------------

// 利用正規表達式改變數據結構

const regName = /(\w+)\s(\w+)/;
const name = 'Smith Adam';
const nameResult = name.replace(regName, '$2 $1');

console.log(nameResult); // Adam, Smith

// 利用正規表達式解析URL

const fakeUrl = 'name=John&age=25&job=engineer';
const reg = /(\w+)=(\w+)/g;

let result;
let obj = {};

while (result !== null) {
  // 每一次exec執行完都會將指標指向結果的lastindex的下一個index，所以可以遍歷
  // 正規表達式記得寫g，不然會無限迴圈！！！
  result = reg.exec(fakeUrl);
  console.log(result);
  if (result !== null) {
    obj[result[1]] = result[2];
  }
}

console.log(obj);
