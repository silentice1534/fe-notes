/**
 * Undefined and Null
 */

// 弱比較時兩個相等
console.log(undefined == null); // true
// 強比較時兩者不相等
console.log(undefined === null); // false

/**
 * 歷史故事
 *
 * C語言中只有null，且也會被轉型為0。
 *
 * JS開發者Brendan Eich設計JS時沒有錯誤處理機制，發生數據不匹配時，會自動轉型或默默地失敗。
 * Brendan Eich覺得null自動轉為0，不容易除錯。
 *
 */

/**
 * 最初設計
 *
 * null表示為無的「物件」，undefined表示為無的「值」
 */

console.log(Number(null)); // 0
console.log(Number(undefined)); // NaN

/**
 * 實際用法，此處為空沒有任何東西
 *
 * null
 *
 * 1.作為函式的參數，表示該函式的參數不是物件
 * 2.作為原型鏈的終點
 * 3.可以作為需要被垃圾回收的標記
 */

console.log(Object.getPrototypeOf(Object.prototype)); // null
// is same as ...
console.log(Object.prototype.__proto__); // null

//###
//

/**
 * undefined
 * 此處應該有個值，但是沒有被定義
 *
 * 1.變數宣告但沒有賦值時
 * 2.該提供給函式的參數沒提供時（似第一種狀況）
 * 3.物件中的屬性沒有被賦值時（似第一種狀況）
 * 4.函數沒有返回值時
 *
 */
