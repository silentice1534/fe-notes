console.log('======千分位逗號======');

// 1. 超暴力解法
function addCommas1(num) {
  return num.toLocaleString();
}

function addCommas2(value) {
  reg = /\d(?=(\d{3})+$)/g;
  const valueStr = String(value);
  if (valueStr.indexOf('.') > -1) {
    const [integer, decimal] = valueStr.split('.');
    return integer.replace(reg, '$&,') + '.' + decimal;
  }

  return valueStr.replace(reg, '$&,');
}

const test1 = 321793812;
const test2 = 321793812.432;
const test3 = -321793812;
const test4 = -321793812.432;

console.log(addCommas1(test1));
console.log(addCommas2(test1));
console.log(addCommas1(test2));
console.log(addCommas2(test2));
console.log(addCommas1(test3));
console.log(addCommas2(test3));
console.log(addCommas1(test4));
console.log(addCommas2(test4));
