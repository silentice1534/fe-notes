/**
 * 參考網址
 * https://kuro.tw/posts/2019/02/23/%E8%AB%87%E8%AB%87-JavaScript-%E7%9A%84-setTimeout-%E8%88%87-setInterval/
 */

/**
 * setTimeout 和 setInterval 的功能幾乎是一樣的，但有個很大的差別
 * 如果setTimeout在5秒鐘後呼叫callback執行需要2秒鐘，那下一次在執行時需要7秒鐘；
 * 但是setInterval沒有這個問題，每次就是間隔五秒執行，不受callback執行時間影響
 */

/**
 * setTimeout的第一個參數可以是字串!
 * 如果直接寫一個statement會直接執行!
 * 寫字串會透過eval轉換為function，效能會比function差
 */

setTimeout(() => {
  console.log('hi'); // 一秒後執行
}, 1000);

setTimeout(console.log('hi'), 1000); // 直接執行

setTimeout("console.log('hi')", 1000); // 一秒後執行

/**
 * 當setInterval遇到迴圈
 * 因為一秒後迴圈已經執行完了，i的值為3
 */

for (var i = 0; i < 3; i++) {
  setTimeout(() => console.log(i), 1000); // 3, 3, 3 // 最後判斷 3 < 3 false,所以i停留在3
}

// 解決方式1. IIFE
for (var i = 0; i < 3; i++) {
  (function(i) {
    setTimeout(() => console.log(i), 1000); // 0, 1, 2
  })(i);
}

// 解決方式2. let，透過let有效範圍為scope的特性
for (let i = 0; i < 3; i++) {
  setTimeout(() => console.log(i), 1000); // 0, 1, 2
}

/**
 * 當setTimeout時間延遲為0時，還是會等待現在stack中執行完才執行，所以一樣會被丟在event quene裡
 */

setTimeout(() => console.log('do after'), 0);
console.log('do first');

// --> 'do first'
// --> 'do after'
