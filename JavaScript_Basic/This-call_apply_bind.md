# call() apply() bind()

主要功能都是用來改變 this 的指向，

這三個與`this`關係深厚，都是 function 的內建立函式

`call()`和`apply()`可以當作呼叫 function 的另一個手段 ；`bind()`會回傳一個包裹後的匿名 function

## call()

#### 功能

- 另一種執行 function 的方式

  可以直接透過()執行 function，但是沒辦法指定 this

- 明確指定 this

#### 語法

`fn.call(this, arg1, arg2, ...)`

上方語法中的 this 位置的物件會被當作目標函式中的 this，若不傳入通常會寫`null`
若不傳入物件、null、undefined，this 會指向 window

#### 使用情境

當參數數量固定時用`call()`

## apply()

#### 功能

- 明確指定 this

#### 語法

`fn.call(this, [arg1, arg2, ...])`

與`call()`相同，但是傳參數的方式必須用陣列，若不是用陣列會跳出錯誤 `CreateListFromArrayLike`

#### 使用情境

當不知道參數的實際數量時可以使用`apply()`

## call()和 apply 小結

使用方式一模一樣，只是傳遞參數不一樣，其中差別的應用在於參數數量是否固定。

參數數量固定 --> 用 call()

參數數量不固定 --> 用 apply()

## bind()

回傳一個包裹後的 function

#### 功能

- 預先設定參數

- 明確指定 this，且為應聯繫

#### 語法

`fn.bind(this, arg1, arg2, ...)`

上方語法中的 this 位置的物件會被當作目標函式中的 this，以硬結繫的方式。

第二個位置之後的參數，作為之後傳進目標函式的參數，所謂之後是指包裹後的 function 被呼叫時。如此一來 this 就不會被改變。

#### 應用

```
// bind 的神奇之處！
function add(a, b) {
  console.log(a + b);
}

console.log(add.call(null, 3, 2)); // 5
console.log(add.apply(null, [7, 1])); // 8
const newAdd = add.bind(this, 1);
console.log(newAdd(2)); // 3
console.log(newAdd(11)); // 12
```

call, apply 的應用都很簡單，在`newAdd`中回傳一個包裹後的 function，add.bind(this, 1)，中的 1 會在被包裹後的 function 被默認當做第一個參數，所以之後只要傳入一個參數就可以了!

參考資料來源

https://ithelp.ithome.com.tw/articles/10195896

https://medium.com/@realdennis/javascript-%E8%81%8A%E8%81%8Acall-apply-bind%E7%9A%84%E5%B7%AE%E7%95%B0%E8%88%87%E7%9B%B8%E4%BC%BC%E4%B9%8B%E8%99%95-2f82a4b4dd66
