# Undefined and Null

若比較時，兩個等價

undefined == null // true



## 歷史故事 ...

在C語言中，只有null，且也會被轉型為0。

JS開發者Brendan Eich設計JS時沒有錯誤處理機制，發生數據不匹配時，會自動轉型或默默地失敗。Brendan Eich覺得null自動轉為0，不容易發生錯誤。

## 最初設計

null表示為無的「物件」，undefined表示為無的「值」

null被轉為number時為0

undefined被轉為number時為NaN

## 實際用法

### null

 - 作為函式的參數，表示該函式的參數不是物件
 - 作為原型鏈的終點 
`Object.getPrototypeOf(Object.prototype)` // null

### undefined

此處應該有個值，但是沒有被定義

 - 變數宣告但沒有賦值時
 `let foo;`
 - 該提供給函式的參數沒提供時（似第一種狀況）
 - 物件中的屬性沒有被賦值時（似第一種狀況）
 - 函數沒有返回值時
  `const f = () => {}; f();` // undefiined

