/**
 * 參考網址: https://juejin.im/post/5dee2194e51d45584221caab
 * 參考網址: https://zhuanlan.zhihu.com/p/34229323
 */

/**
 * 為了讓JS運行起來，需要兩個主要工作
 *
 * JavaScript Engine(V8、Trident、Gecko等等):
 * 編譯並執行JS、分配內存、垃圾回收等
 *
 * JavaScript Runtime(執行環境，也就是瀏覽器Chrome、IE等等):
 * 為JS提供一些物件和機制，讓他可以與外界互動
 *
 * JavaScript Engine實現了ECMAScript的標準中的數據類型、操作符號、物件、方法等等，
 * 而JavaScript Runtime則提供Window、DOM物件等等
 *
 * JavaScript Runtime提供的物件說明了為什麼Chrome、IE和其他各種瀏覽器會有不同的語法，
 * e.g.addEventListen、attachEvent
 */

/**
 * JavaScript Engine會維護一個Heap和一個Stack，然後通過JavaScript Runtime提供的API執行腳本
 * JS 只有一個主線程，所以如果有一個任務耗時間，就會阻塞後面的腳本！
 *
 * 為了避免阻塞，JS Runtime有另外三個線程(也就是JS有主線程、瀏覽器提供其他線程)，讓JS有能力達到非阻塞、非同步地執行腳本，這三個線程分別為:
 * 1. 事件觸發、
 * 2. 定時器、
 * 3. HTTP請求
 *
 * 這就是JS併發模型(Concurrency Model)的基礎
 */
