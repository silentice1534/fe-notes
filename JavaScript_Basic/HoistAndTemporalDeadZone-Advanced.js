/**
 * 參考文章
 * https://blog.techbridge.cc/2018/11/10/javascript-hoisting/
 */

/**
 * 1. Execution Context and Variable Object
 * 2. Is Javascript a pure interpreted language ?
 */

// ---------------------------------------------------

/**
 * Execution Context(執行環境) and Variable Object
 */

/**
 * 當進入一個function，系統就會產生一個新的Execution Context，最下面是Global exection context
 * ----------------------------
 * | execution context        |
 * ----------------------------
 * | execution context        |
 * ----------------------------
 * | execution context        |
 * ----------------------------
 * | global execution context |
 * ----------------------------
 *
 * 每一個execution context，都有對應的variable object。
 * 在function中的arguments, variable, function都會被加進variable object，
 * 若參數沒有值設定為undefined，
 * 若有同名的變數就覆蓋掉，
 * 若要使用時在variable object中找不到，就透過scope chain往外找。
 */

(function(x, y) {
  console.log(x, y);
})();

/**
 * Is Javascript a pure interpreted language ?
 */

/**
 * 編譯語言與直譯語言
 *
 * 編譯語言需要將程式碼轉換成機器碼，這個過程叫編譯語言，如此才能讓電腦運作，所以效率更快。
 * 直譯語言則是針對程式碼逐行執行，因為邊解析邊執行，所以效率較編譯語言差。
 */

/**
 * 當我們說javascript直譯語言時，是指大部分的時候，而hoisting這件事情就是在編譯這個階段處理的。
 * 現在知道Javascript並不是完全的直譯語言後，可以知道將javascript分成編譯階段與直譯階段，
 * 且，若沒有經過編譯階段，是不可能有hoisting的狀況的的。
 *
 * 為了改善編譯語言與直譯語言的缺點，發展出即時編譯的技術，這種混合了編譯語言與直譯語言的優點，稱為中介碼(byte code);
 * 如此會比純編譯語言慢一些，但有直譯語言的特色。
 *
 * byte code部分更深入了，可參考「參考文章」連結，最後一部份。
 *
 */
