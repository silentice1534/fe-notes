/**
 * 參考網站
 *
 * https://dotblogs.com.tw/acelee/2017/06/22/135553
 * https://pjchender.blogspot.com/2016/06/javascriptfunction-constructornew.html
 * https://www.tutorialsteacher.com/javascript/new-keyword-in-javascript
 */

/**
 * new operator
 *
 * 參考了JAVA語言建構類別的方式
 *
 * 1. 函式建構式
 * 2. new 做了什麼事
 */

/**
 * 函式建構式
 *
 * 就是一個function !
 * 建立函式建構式時，約定俗成會使用大寫開頭。
 */

/**
 * new 做了什麼事
 *
 * 通過new建構出來的物件，我們稱為實例
 *
 * 1. 建立一個空物件O
 * 2. 將prototype指向__proto__，接起原型鏈
 * 3. 將this指到這個物件，並將屬性方法指到這個O物件
 * 4. 回傳這個物件
 *
 *
 * 在建構式下默認 return this
 
 */

// 在建構式下默認 return this
function Person(name, age) {
  this.name = name;
  this.age = age;
}

const P = new Person('John', 23);
console.log(P); // {name: 'John', age: 23}

// 若自己return其他物件，會被這個物件取代，
function Person2(name, age) {
  this.name = name;
  this.age = age;
  return {
    nothing: 'this object will cover constructor',
  };
}

const P2 = Person2('Wendy', 34);
console.log(P2); // {nothing: 'this object will cover constructor'}
