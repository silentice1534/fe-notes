const xhr = new XMLHttpRequest();
const apiUrl = 'https://jsonplaceholder.typicode.com/todos/1';
const method = 'GET';

xhr.open(method, apiUrl);

// 設定header
xhr.setRequestHeader('auth', 'there!');
// 如果有是POST 就會有body
const payload = JSON.stringify({ name: 'Mike' });
xhr.send(payload);

xhr.onreadystatechange = () => {
  if (xhr.readyState === 4) {
    if (xhr.status >= 200 || xhr.status < 400) {
      const res = xhr.responseText;
      const data = JSON.parse(res);
      console.log(data);
    }
  }
};
