[參考網址 1](https://juejin.im/post/5c4528a6f265da611a4822cc_)
[參考網址 2](https://blog.techbridge.cc/2017/06/17/cache-introduction/)

- DNS

  看 **dns.md**

- CDN

  找最近的地方連線!

- 瀏覽器

  1. `Expires` http 1.0

  在 http header 中加上 Expires，顯示大概如下 `Expires: Wed, 21 Oct 2017 07:28:00 GMT`，
  若沒有超過時間，會從電腦的硬碟讀取資料，若手動設定電腦時間為 2100 年，會重新發 request

  2. `Cache-Control` 和 `max-age` --- http 1.1

  `Expires` 為了解決使用這電腦未來時間的問題而出現了`Cache-Control`
  `Cache-Control`顯示如 `Cache-Control: max-age: 30`，數字是秒數

  **如果 Expires 和 Cache-Control，會使用 Cache-Control**

  過期了 ... 但說不定還是能使用

  3. `Last-Modified` 與 `If-Modified-Since` --- http1.0

  需要前後端配合

  由後端回應時多加一個`Last-Modified`header，下次前端再取資料時，header 加上`If-Modified-Since`，如果是用
  `Last-Modified`快取起來的會回應 304(Not Modified)

  4. `Etag`與`If-None-Match`

  上面的是檔案有沒有被編輯過，但是如果只是打開又存檔，編輯時間又會改變。所以進一步判斷檔案是否有被編輯過。
  後端回應時加一個`Etag`，前端詢問用`If-Modified-Since`如果被編輯過會產生新的 hash 值，用`Etag`快取起來一樣是回應 304

  `Expires`和 `Cache-Control` 和 `max-age` 用來判斷檔案夠不夠新。

  `Last-Modified` 與 `If-Modified-Since`和 `Etag`與`If-None-Match`則詢問伺服器有沒有新的資源。

  5. 不要快取時

  下面兩個語法不要任何快取

  5-1. `Cache-Control: no-store` --- http1.0
  5-2. `Pragma: no-cache` --- http1.1

  6. 如果要網站每次更新都及時更新

  可以用下面這種方式
  後端

  ```
  Cache-Control: max-age=0
  Etag: 123
  ```

  ```
  If-None-Match: 1234
  ```

  這算是一種變通方式，但其實有另一種作法

  `Cache-Control: no-cache`

  7. `Cache-Control: no-store`和 `Cache-Control: no-cache`

  `Cache-Control: no-store`: 是完全不使用快取，每次都抓新的回來
  `Cache-Control: no-cache`: 每次都去看有沒有資料變更，有就抓新的，沒有就回應 304
