class CopyText extends HTMLElement {
  constructor() {
    super();
    const rootShadow = this.attachShadow({ mode: 'open' });
    const template = document.querySelector('#copy-text-btn');
    const cloneTemplate = template.content.cloneNode(true);
    rootShadow.append(cloneTemplate);
    this.shadowDOM = rootShadow;
  }

  static get observedAttributes() {
    return ['copyid'];
  }

  get copyid() {
    return this.hasAttribute('copyid');
  }

  set copyid(val) {
    if (val) {
      this.setAttribute('copyid', val);
    } else {
      this.removeAttribute('copyid');
    }
  }

  attributeChangedCallback() {
    console.log(this.copyid);
    if (this.copyid) {
      this.handleCopyText();
    }
  }

  handleCopyText() {
    // 沒辦法成功，因為document.存取不到 subtree裡面的input，也就沒辦法copy!
    const id = this.getAttribute('copyid');
    const inputEle = this.shadowDOM.querySelector('input');
    inputEle.value = document.getElementById(id).innerHTML;
    inputEle.select();
  }
}
customElements.define('copy-text-btn', CopyText);

console.log(document.execCommand('Copy'));

function handleClick() {
  console.log(document.getElementById('test'));
}
