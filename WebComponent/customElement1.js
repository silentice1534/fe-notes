class formatNum extends HTMLElement {
  constructor() {
    super();
    // 建立一個root，用來掛載template用的
    let shadowRoot = this.attachShadow({ mode: 'open' });

    const t = document.querySelector('#format-num-template');
    // 拷貝 template 這個節點
    const instance = t.content.cloneNode(true);
    shadowRoot.appendChild(instance);
    this.shadowDOM = shadowRoot;
  }

  static get observedAttributes() {
    return ['digits', 'num', 'si'];
  }

  // 取得屬性並返回值
  get digits() {
    return this.hasAttribute('digits');
  }

  set digits(val) {
    if (val) {
      this.setAttribute('digits', val);
    } else {
      this.removeAttribute('digits');
    }
  }
  get num() {
    return this.hasAttribute('num');
  }

  set num(val) {
    if (val) {
      this.setAttribute('num', val);
    } else {
      this.removeAttribute('num');
    }
  }

  get si() {
    return this.hasAttribute('si');
  }

  set si(val) {
    if (val) {
      this.setAttribute('si', val);
    } else {
      this.removeAttribute('si');
    }
  }

  attributeChangedCallback() {
    if (this.digits || this.num) {
      this.formatNum();
    }
  }

  format(num, digits, si) {
    return num > 999 ? (num / 1000).toFixed(digits) + 'k' : num;
  }

  formatNum() {
    console.log('?');
    const num = this.getAttribute('num');
    const digits = this.getAttribute('digits');
    const si = this.hasAttribute('si');
    this.shadowDOM.querySelector('slot').textContent = this.format(
      num,
      digits,
      si,
    );
  }
}
customElements.define('format-num', formatNum);
