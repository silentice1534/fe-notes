## 比較 RWD 和 AWD

|            | RWD                                            | AWD                                        |
| ---------- | ---------------------------------------------- | ------------------------------------------ |
| 差異       | 根據螢幕寬度計算 CSS                           | 網頁載入後判斷裝置寬度載入特定寬度         |
| 取捨       | RWD 的畫面基本上一樣                           | AWD 會根據裝置做優化，減少不必要的圖文載入 |
| 開發時     | 比較方便，只要維護一套                         | AWD 每一個版本都是分開開發，成本較高       |
| 上線後維護 | 調整會花多一點時間，因為可能牽動其他寬度的屬性 | 維護相對容易，因為檔案分開了               |
| 如何選擇   | 畫面內容較少的，官網類型                       | 資訊內容多的，如網購平台                   |
| UX 感受    | AWD>RWD                                        |
