- 解釋 CSS 動畫與 JavaScript 動畫之間的憂與劣。

  參考: https://developers.google.com/web/fundamentals/design-and-ux/animations/css-vs-javascript?hl=zh-tw

  1. 使用情境
     1-1. CSS
     1-1-1. 一次性的轉換，e.g. UI 切換
     1-1-2. 元素較小時、自成一體的 UI
     1-2. JS
     1-2-1. 需要有效操控動畫時
     1-2-2. 元素架構較複雜時
  2. 如果使用 JS，可優先考慮 TweenMax、TweenLite(較輕量)

- CSS 動畫和 JS 動畫效能

  參考: https://developers.google.com/web/fundamentals/design-and-ux/animations/animations-and-performance?hl=zh-tw#css-vs-javascript-performance

  1. CSS 動畫是由另一個瀏覽器主執行緒以外的執行緒處理，JS 則是在主執行緒上
  2. 如果動畫觸發了 reflow，會讓主執行緒必須工作!
  3. 對需要跑動畫的屬性加上 will-change
