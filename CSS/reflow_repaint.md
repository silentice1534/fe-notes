[參考網址](https://juejin.im/post/5a9923e9518825558251c96a)

## 簡單渲染概念

- 瀏覽器的佈局是流式的
- 瀏覽器會把 HTML 解析成 DOM、把 CSS 解析成 CSSOM，合併成 render tree
- 有了 render tree，就可以計算在頁面上的大小和位置，最後 render 到畫面是
- 渲染 table 的計算比較複雜，所以盡量避免用 table

**reflow 必定引起 repaint， repaint 不易定會引起 reflow**

故 reflow 的成本大於 repaint

## reflow

尺寸或結構改變時，就會觸發 reflow!

會導致 reflow 的原因如下:

- 頁面首次渲染
- 瀏覽器尺寸改變
- 元素尺寸或位置改變
- 元素內容變化，如文字、圖片
- 增加或刪除**可見**的 DOM 元素
- 觸發偽累，如:hover
- 查詢或呼叫某些方法，如下

屬性

- clientWidth、clientHeight、clientTop、clientLeft
- offsetWidth、offsetHeight、offsetTop、offsetLeft
- scrollWidth、scrollHeight、scrollTop、scrollLeft

方法

- scrollIntoView()、scrollIntoViewIfNeeded()
- getComputedStyle()
- getBoundingClientRect()
- scrollTo()

## repaint

頁面中的樣式屬性改變時，不影響尺寸和大小時，如 color、background-color、visibility

## 避免 reflow 和 repaint

- 盡量避免使用 table
- 盡可能在 DOM 的最末端改變 CSS(不要頻繁操作父元素)
- 巢狀盡可能少一點
- 動畫效果盡量移動到 position 屬性為 absolute 和 fixed 上
- 避免適用`calc()`
