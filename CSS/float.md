### float 如何運作

- 排版看 **/css/float-layout.html**
- 其他特性看 **/css/float-other.html**

### 為什麼要清除浮動，什麼時候要清除浮動，有哪些方法

- float 會導致高度塌陷，高度塌陷是指，父元素原本應該包和子元素的高度，但是卻沒包含
- 清除浮動的方式，看 **/css/float-clear-height-collapse.html**
