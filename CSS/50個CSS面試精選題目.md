[原文](https://zhuanlan.zhihu.com/p/75132250)

1 介绍一下标准的 CSS 的盒子模型？与低版本 IE 的盒子模型有什么不同的？

2 box-sizing 属性？

3 CSS 选择器有哪些？哪些属性可以继承？

4 CSS 优先级算法如何计算？

5 CSS3 新增伪类有那些?

6 如何居中 div？如何居中一个浮动元素？如何让绝对定位的 div 居中？(code)

7 display 有哪些值？说明他们的作用?

8 position 的值？

9 CSS3 有哪些新特性？

10 请解释一下 CSS3 的 flexbox（弹性盒布局模型）,以及适用场景？

11 用纯 CSS 创建一个三角形？(code)

~~12 一个满屏品字布局如何设计?~~

**13 常见的兼容性问题？**

14 为什么要初始化 CSS 样式

**15 absolute 的 containing block 计算方式跟正常流有什么不同？(寫 code 理解一下)**

~~16 CSS 里的 visibility 属性有个 collapse 属性值？在不同浏览器下以后什么区别？~~

17 display:none 与 visibility：hidden 的区别？

_18 position 跟 display、overflow、float 这些特性相互叠加后会怎么样？(不知在說啥)_

**19 对 BFC 规范(块级格式化上下文：block formatting context)的理解？**

20 为什么会出现浮动和什么时候需要清除浮动？清除浮动的方式？

21 上下 margin 重合的问题

22 设置元素浮动后，该元素的 display 值是多少？

~~23 移动端的布局用过媒体查询吗？~~

24 使用 CSS 预处理器吗？

**25 CSS 优化、提高性能的方法有哪些？**
