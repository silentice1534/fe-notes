## 何謂 flex

腦中那張圖!

## flex-grow, flex-shrink, flex-basis

1. flex-grow 用於當所有 flex items 總寬小於 flex contianer 時，按給各個 flex item 定比例分配;
2. 若所有 flex items 大於 flex container，flex-grow 無效
3. flex-shrink 用於當所有 flex items 總寬大於於 flex contianer 時，按給各個 flex item 定比例壓縮;
4. 若所有 flex items 小於 flex container，flex-shirnk 無效
5. flex-basis: 佔用 main-axis 的尺寸
6. flex-basis: auto  時，會根據 width 設定，如下優先度
7. flex-basis > width: oo px > width: auto
8. flex: 1 的縮寫來自 flex-grow: 1, flex-shrink: 1; flex-basis: 0%;
