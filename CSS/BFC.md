## BFC(Block Formatting Context)

- 是一個獨立的渲染區域(獨立容器)，只有塊級元素參與，規定了塊級元素如何渲染
- 可以簡單理解為某個 CSS 屬性，但這個屬性無法被開法者使用/修改
- 滿足下列條件之一會會生成 BFC
  1.   根元素，即 html 元素
  2.  float 的值不為 none
  3.  overflow 的值不為 visible 時
  4.  display 的值為 inline-block, table-cell, table-caption
  5.  position 的值為 absolute 或 fixed
- BFC 佈局規則
  1.  內部的 box 會是垂直方向，一個接一個放
  2.  垂直方向的距離由 margin 決定，相鄰的 margin 會重疊
  3.  BFC 是頁面上的獨立容器，內外部的元素不會互相影響
  4.  計算 BFC 高度時，浮動的高度也會算進去(即一般沒有浮動的狀況下)
- BFC 的用途
  1.  避免元素被浮動元素覆蓋
  2.  可以清除內部浮動
  3.  分屬於不同 BFC 時可以阻止 margin 重疊
  4.  兩欄自適應

**BFC 是頁面上的獨立容器，內外部的元素不會互相影響**，可以解釋為何可以讓元素不被覆蓋以及可以清除內部浮動
