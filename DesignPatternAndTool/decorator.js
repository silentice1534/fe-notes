Function.prototype.before = function(fn) {
  const self = this;

  return function() {
    if (!fn.apply(this, arguments)) {
      return;
    } else {
      self.apply(this, arguments);
    }
  };
};

Function.prototype.after = function(fn) {
  const self = this;

  return function() {
    const result = self.apply(this, arguments);
    fn.apply(this, arguments);

    return result;
  };
};

function isNumber(val) {
  console.log('this is before fn...');
  if (typeof val !== 'number') {
    console.log('驗證錯誤');
    return false;
  }

  return true;
}

function echoEnd() {
  console.log('this is after fn...');
}

function doSomething() {
  console.log('do something ...');
  return 'can you see me ?';
}

const result1 = doSomething.before(isNumber);
const result2 = doSomething.after(echoEnd);

result1(111);
result2(111);

// 如果要執行前後各有一個執行function的話

Function.prototype.beforeAfter = function(beforeFn, afterFn) {
  const self = this;

  return function() {
    beforeFn.apply(this, arguments);
    const result = self.apply(this, arguments);
    afterFn.call(this, result);
  };
};

const result3 = doSomething.beforeAfter(isNumber, echoEnd);
result3();
