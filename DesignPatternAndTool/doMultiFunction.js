function fn1() {
  console.log('echo 1');
}

function fn2() {
  console.log('echo 2');
}

function fn3() {
  console.log('echo 3');
}

function doMultiFunction(initVal) {
  return function() {
    let result = initVal;
    for (const fn of arguments) {
      result = fn.apply(this, arguments);
    }

    return result;
  };
}

const exec = doMultiFunction()(fn1, fn2, fn3);
console.log(exec());
