function singleton(fn) {
  let instance;
  return function() {
    return instance || (instance = fn.apply(this, arguments));
  };
}

function createDiv1(w, h) {
  const ele = document.createElement('div');
  ele.style.width = w + 'px';
  ele.style.height = h + 'px';
  ele.style.backgroundColor = '#ccc';
  document.body.appendChild(ele);

  return ele;
}

function createDiv2(w, h) {
  const ele = document.createElement('div');
  ele.style.width = w + 'px';
  ele.style.height = h + 'px';
  ele.style.backgroundColor = '#aaa';
  document.body.appendChild(ele);

  return ele;
}

const div1 = singleton(createDiv1);
const div2 = singleton(createDiv2);

console.log(div1(100, 200));
console.log(div2(300, 400));
console.log(div1(100, 200)); // 直接返回原已創建的div
console.log(div2(300, 400)); // 直接返回原已創建的div
