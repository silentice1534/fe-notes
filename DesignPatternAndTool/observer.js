const observer = {
  // 儲存所有訂閱的callback
  // 長得像如果有訂閱，會長得像
  // clientList: [
  //   login: [fn1, fn2,fn3],
  //   logout: [fn1, fn2,fn3]
  // ]
  clientList: {},
  listen(key, fn) {
    if (!this.clientList[key]) {
      this.clientList[key] = [];
    }

    console.log();

    this.clientList[key].push(fn);
  },
  trigger() {
    const key = Array.prototype.shift.call(arguments);
    const fnList = this.clientList[key];

    if (!fnList || fnList.length === 0) {
      console.log('沒有此訂閱');
      return;
    }

    for (const fn of fnList) {
      fn.apply(this, arguments);
    }
  },
  remove(key, fn) {
    const fnList = this.clientList[key];

    // 如果沒有對應的訂閱，直接返回
    if (!fnList) {
      return;
    }

    // 如果沒有傳入此特定fn，刪除所有fn
    if (!fn) {
      fns && (fns.length = 0);
    }

    // 刪除特定fn
    for (let i = 0; i <= fnList.length; i++) {
      const _fn = fnList[i];
      if (fn === _fn) {
        fnList.splice(i, 1);
      }
    }
  }
};

// foo 模組
const foo = (function() {
  observer.listen('whenLogin', (foofn = data => foo.execFn(data)));

  return {
    execFn(data) {
      console.log(data);
    }
  };
})();

const boo = (function() {
  observer.listen('whenLogin', (boofn = data => boo.execFn(data)));

  return {
    execFn(data) {
      console.log(data);
    }
  };
})();

observer.remove('whenLogin', boofn);

setTimeout(() => {
  observer.trigger('whenLogin', 'login success!');
}, 1000);
