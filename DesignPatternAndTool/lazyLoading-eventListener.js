let addEvent = function(ele, type, fn) {
  if (window.addEventListener) {
    addEvent = function(ele, type, fn) {
      ele.addEventListener(type, fn, false);
    };
  } else {
    addEvent = function(ele, type, fn) {
      ele.attachEvent('on' + type, fn);
    };
  }

  addEvent(ele, type, fn);
};

const btn = document.getElementById('ll-btn');

addEvent(btn, 'click', () => console.log('hi'));
addEvent(btn, 'click', () => console.log('hi'));
