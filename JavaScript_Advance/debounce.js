function debounce(fn, delay) {
  let timer = null;

  return () => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(this, arguments);
    }, delay);
  };
}

function fn() {
  console.log('exec fn ...');
}

const box = document.getElementById('box');
box.addEventListener(
  'mouseenter',
  debounce(() => fn(), 500)
);
