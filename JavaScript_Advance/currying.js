// 參考 https://cythilya.github.io/2017/02/27/currying-in-javascript/
// 參考 https://juejin.im/post/5af13664f265da0ba266efcf

// 將一個接受多個參數的函式，轉變成數個只接受一個參數的函式
// 原理是利用閉包
// 好處 1. 簡化參數的處理，基本上一次處理一個參數，可提高函式的可讀性
// 好處 2. 將程式碼一功能拆解成更細的片段，利於重複使用

console.log('=====CURRY=====');

// e.g.
function multiply(x, y) {
  return x * y;
}

function multiplyCurry(x) {
  return function(y) {
    return x * y;
  };
}

console.log(multiply(2, 3));
console.log(multiplyCurry(2)(3));

// 實現 currying 函式

function currying(fn, ...args) {
  if (args.length >= fn.length) {
    return fn(...args);
  }

  return (...args2) => {
    return currying(fn, ...args, ...args2);
  };
}

console.log(currying(multiply, 2)(3));
