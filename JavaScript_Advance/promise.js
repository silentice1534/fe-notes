function PromiseFn(executor) {
  // 初始化state为等待态
  this.state = 'pending';
  // 成功的值
  this.value = undefined;
  // 失败的原因
  this.reason = undefined;
  // 存放 fn1 的回调
  this.fn1Callbacks = [];
  // 存放 fn2 的回调
  this.fn2Callbacks = [];
  // 成功
  let resolve = value => {
    if (this.state === 'pending') {
      this.state = 'fulfilled';

      this.value = value;
    }
  };
  // 失败
  let reject = reason => {
    if (this.state === 'pending') {
      this.state = 'rejected';

      this.reason = reason;
    }
  };
  // 立即执行

  try {
    executor(resolve, reject);
  } catch {
    reject();
  }
}

PromiseFn.prototype.then = function(fn1, fn2) {
  const self = this;
  let promiseF;

  fn1 = typeof fn1 === 'function' ? fn1 : function() {};
  fn2 = typeof fn2 === 'function' ? fn2 : function() {};

  if (self.state === 'fulfilled') {
    return (promise2 = new Promise(function(resolve, reject) {
      try {
        var x = fn1(self.data);
        resolve(x);
      } catch (e) {
        reject(e);
      }
    }));
  }

  if (self.state === 'rejected') {
    return (promise2 = new Promise(function(resolve, reject) {
      try {
        var x = fn2(self.data);
        reject(x);
      } catch (e) {
        reject(e);
      }
    }));
  }

  if (self.state === 'pending') {
    return (promise2 = new Promise(function(resolve, reject) {
      this.fn1Callback.push(function(value) {
        try {
          var x = fn1(self.data);
          resolve(x);
        } catch (e) {
          reject(e);
        }
      });
      this.fn2Callback.push(function(value) {
        try {
          var x = fn2(self.data);
          reject(x);
        } catch (e) {
          reject(e);
        }
      });
    }));
  }
};
