// 連續function串連
// 給定任意多個function，將每個執行結果回傳

const multiply5 = function(x) {
  return x * 5;
};

const add10 = function(x) {
  return x + 10;
};

const divide2 = function(x) {
  return x / 2;
};

const compose = function(initVal) {
  let result = initVal;

  return function(...args) {
    args.forEach(fn => {
      console.log(fn);
      result = fn(result);
    });

    return result;
  };
};

const composeCollection = compose(10);
composeCollection(multiply5, add10, divide2);
