function throttle(fn, delay) {
  let timer = null;
  let canExec = true;

  return function() {
    if (!canExec) return;

    canExec = false;
    fn.apply(this, arguments);
    timer = setTimeout(() => {
      canExec = true;
      clearTimeout(timer);
    }, delay);
  };
}

function fn() {
  console.log('exec fn ...');
}

const box = document.getElementById('box');
box.addEventListener('mousemove', throttle(fn, 1000));
