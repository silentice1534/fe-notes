// 1. 超暴力解法
console.log('======千分位逗號======');
let num = 321793812.432;

function addCommas(num) {
  if (typeof num === 'number' && !isNaN(num)) {
    return num.toLocaleString();
  }

  throw new Error('參數型別需為數字');
}
