// 模擬一個 new 運算子

/**
 * 1. 建立一個新物件
 * 2. 將prototype指向__proto__
 * 3. this指到新物件
 * 4. 回傳這個物件
 */
console.log('=====NEW=====');

// ES5
function newFactory() {
  const obj = {};
  const constructor = Array.prototype.shift.call(arguments);
  obj.__proto__ = constructor.prototype;

  const result = constructor.apply(obj, arguments);
  const checkType = Object.prototype.toString.call(result);

  return checkType === '[object Object]' ? result : obj;
}

function Person(name) {
  this.name = name;
}

// ES6

function newFactory2(constructor, ...rest) {
  const obj = Object.create(constructor.prototype);
  const result = constructor.apply(obj, rest);
  return typeof result === 'object' ? result : obj;
}

const Jack = newFactory(Person, 'Jack');
const Jack2 = newFactory2(Person, 'Jack2');
console.log(Jack.name);
console.log(Jack2.name);
