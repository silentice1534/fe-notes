// 模擬一個 instanceof function
console.log('=====INSTANCE OF=====');
class Car {
  constructor(name) {
    this.name = name;
  }
}

const benz = new Car('benz');

function instanceofFn(target, origin) {
  // 若基本類型為null 或不是物件或 直接返回false
  if (typeof target !== 'object' || target === null) {
    return false;
  }

  const originPrototype = origin.prototype;
  target = target.__proto__;

  while (target) {
    if (target === null) {
      console.log(111);
      return false;
    }

    if (target === originPrototype) {
      console.log(222);
      return true;
    }

    target = target.__proto__;
  }
}

console.log(instanceofFn(benz, Car));
console.log(instanceofFn(toString, Object));
