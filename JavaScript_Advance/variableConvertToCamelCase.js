console.log('=====TO CAMEL CASE=====');
function toCamel(variable) {
  // 驗證格式對不對
  const regExp = /[^A-Za-z\-]/;
  const isValidate = regExp.test(variable);
  if (isValidate) {
    return '格式錯誤';
  }

  return variable.replace(/-\w/g, matchValue => {
    return matchValue.slice(1).toUpperCase();
  });
}

console.log(toCamel('how-are-you'));
