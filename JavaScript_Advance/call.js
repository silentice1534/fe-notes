console.log('=====CALL=====');
Function.prototype.callFn = function(context) {
  // this 現在指到執行環境，也就是 echo 這個 function
  // 所以在 context 上增加一個被指到的function
  context.fn = this || window;

  // 將arguments轉為陣列，並刪除第一個參數，再分解為參數
  let args = Array.from(arguments).slice(1);

  const result = context.fn(...args);
  delete context.fn;
  return result;
};

const testObj = {
  value: 'testObj',
  echo: function(name, n, b) {
    console.log(this.value);
    console.log(name, n, b);
  }
};

const obj = {
  value: 'obj'
};

testObj.echo();
testObj.echo.call(obj);
testObj.echo.callFn(obj, 'Jeff', 3, false);
