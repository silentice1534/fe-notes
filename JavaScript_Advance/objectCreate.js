function createFn(obj, props) {
  function F() {}
  F.prototype = obj;
  let o = new F();

  if (typeof props === 'object') {
    Object.defineProperties(o, props);
  }
  return o;
}

const o1 = Object.create({}, { year: { value: 2019 } });
const o2 = createFn({}, { year: { value: 2019 } });

console.log(o1);
console.log(o2);
