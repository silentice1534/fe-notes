console.log('=====APPLY=====');
Function.prototype.applyFn = function(context) {
  // this 現在指到執行環境，也就是 echo 這個 function
  // 所以在 context 上增加一個被指到的function
  context.fn = this || window;

  // 將arguments轉為陣列，並刪除第一個參數，再分解為參數
  // let args = Array.from(arguments[1]);

  // console.log(args);
  console.log(arguments[1]);

  if (arguments[1]) {
    return context.fn(...arguments[1]);
  } else {
    return context.fn();
  }
};

const testObj = {
  value: 'testObj',
  echo: function(name, n, b) {
    console.log(this.value);
    console.log(name, n, b);
  }
};

const obj = {
  value: 'obj'
};

testObj.echo();
testObj.echo.apply(obj, ['Jeff', 3, false]);
testObj.echo.applyFn(obj, ['Jeff', 3, false]);
