

const result = axios({
  methods: 'get',
  url:'https://jsonplaceholder.typicode.com/todos/1'
})

new Promise((resolve) => {
  setTimeout(() => {
    resolve(true)
  } ,1000)
}).then((status) => {
  if(status) {
    console.log('status...', status);
    result.then((val) => {
      console.log('val...', val)
    })
  } else {
    console.log('nothing');
  }
})