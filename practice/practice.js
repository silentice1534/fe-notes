// 鏈式

const calcFn = function() {
  this.arr = [];
};

calcFn.prototype.cloneArr = function(arr) {
  this.arr = Object.assign([], arr);
  return this;
};

calcFn.prototype.multiply2 = function() {
  const resultArr = [];
  for (const ele of this.arr) {
    resultArr.push(ele * 2);
  }

  return resultArr;
};

calcFn.prototype.divide2 = function() {
  const resultArr = [];
  for (const ele of this.arr) {
    resultArr.push(ele / 2);
  }

  return resultArr;
};

const calc = new calcFn();

console.log(calc.cloneArr([2, 3, 4]).multiply2());
console.log(calc.cloneArr([2, 3, 4]).divide2());
