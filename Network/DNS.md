[參考網址 1](http://dns-learning.twnic.net.tw/dns/01whatDNS.html)
[參考網址 2](https://zh.wikipedia.org/wiki/%E5%9F%9F%E5%90%8D%E7%B3%BB%E7%BB%9F)
[參考網址 3](https://blog.csdn.net/cat_foursi/article/details/71194397)
[參考網址 4](https://juejin.im/post/5c4528a6f265da611a4822cc)

## DNS 是什麼

DNS(Domain Name System 或 Domain Name Service)是一個系統軟體。主要作用是將 domain name 和 IP address 互相轉換。其主要目的是因為電腦只看得懂 0 和 1，而人對有意義的文字比較有記憶，所以提供一個電腦和人類互相轉換的功能。當在網址列中輸入網址或是 IP 時就已經在使用 DNS 服務了。

## 正解析/反解析

正解析 Forward domain --> domain name to IP address
反解析 Reverse domain --> IP address to domain name

## 正確的域名

正確的域名最後會有一個點，例如 yahoo.com.tw. 早期輸入時需要連同最後一個點一起輸入在網址列中，DNS 才會解析。
現在的 DNS 系統已經會自動補上，所以不需要自己打了。

## DNS 的查詢過程

- 搜尋瀏覽器自己的 dns 快取
- 讀取操作系統的 hosts 文件查詢，是否存在對應搜尋網域對應的 IP 的關係(這句有點不懂)
- 查詢本地 DNS(ISP 或是自己手動設定的)
- 向根伺服器發送請求，進行遞歸查詢

上面編號表依序查詢，任何一個有找到，就停止
